import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsHelperService } from '@appCore/services/form-helper/form-helper.service';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { ToastsDocumentService } from '@appCore/services/toasts/toasts-document.service';
import { getPopupIsOpened } from '@appCore/store/popup';
import * as fromDocumentPackageStore from '@appDocuments/core/store/document-package';
import { getIsDocumentPackageLoading } from '@appDocuments/core/store/document-package';
import { getRegistrationNumber } from '@appDocuments/core/store/registration-number';
import { DocumentCreateFormService } from '@appDocuments/shared/services/document-create-form.service';
import { TestUtils } from '@jestConfig/test-utils';
import { of } from 'rxjs';
import { instance, mock, when } from 'ts-mockito';
import { DocumentCreateComponent } from './document-create.component';
import { SavePackageMapperService } from './services/save-package-mapper.service';
import { LetDirective } from '@appShared/directives/let.directive';
import { DocumentsSidebarLogicService } from '@appDocuments/core/components/documents-sidebar/documents-sidebar-logic.service';

describe('DocumentsAboutComponent', () => {
  let comp: DocumentCreateComponent;
  let fixture: ComponentFixture<DocumentCreateComponent>;

  let reduxServiceMock: ReduxService;
  let formsHelperServiceMock: FormsHelperService;
  let asideLogicMock: DocumentsSidebarLogicService;

  const registrationNumber = '423';

  beforeEach(() => {
    const activatedRouteStub = {
      params: {
        pipe: () => ({
          subscribe: () => ({}),
        }),
      },
    };
    const documentCreateFormServiceStub = {
      createDocumentsAboutForm: () => ({}),
      isPmOrPpmMeeting: () => ({}),
    };
    const savePackageMapperServiceStub = {
      mapFormToSaveRequest: () => ({}),
    };
    const routerStub = {
      navigateByUrl: () => {},
      events: of(),
    };

    const toastStub = {
      addToast: () => {},
    };

    reduxServiceMock = mock(ReduxService);
    formsHelperServiceMock = mock(FormsHelperService);
    formsHelperServiceMock = mock(FormsHelperService);
    asideLogicMock = mock(DocumentsSidebarLogicService);

    TestBed.configureTestingModule({
      declarations: [DocumentCreateComponent, LetDirective],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: FormsHelperService, useFactory: () => instance(formsHelperServiceMock) },
        { provide: ReduxService, useFactory: () => instance(reduxServiceMock) },
        { provide: DocumentCreateFormService, useValue: documentCreateFormServiceStub },
        { provide: SavePackageMapperService, useValue: savePackageMapperServiceStub },
        { provide: Router, useValue: routerStub },
        { provide: ToastsDocumentService, useValue: toastStub },
        { provide: DocumentsSidebarLogicService, useFactory: () => instance(asideLogicMock) },
      ],
    });

    when(reduxServiceMock.selectStore(getIsDocumentPackageLoading)).thenReturn(of());
    when(reduxServiceMock.selectStore(getPopupIsOpened)).thenReturn(of(true));
    when(reduxServiceMock.selectStore(getRegistrationNumber)).thenReturn(of(registrationNumber));
    when(reduxServiceMock.selectStore(fromDocumentPackageStore.getDocumentPackage)).thenReturn(of());
    when(reduxServiceMock.selectStore(fromDocumentPackageStore.getDocumentPackageError)).thenReturn(of());

    fixture = TestBed.createComponent(DocumentCreateComponent);
    comp = fixture.componentInstance;
  });

  it('can load instance', () => {
    expect(comp).toBeTruthy();
  });

  it('snapshot', () => {
    const el = TestUtils.removeSnapshotAttrs(fixture.nativeElement);
    expect(el).toMatchSnapshot();
  });
});
