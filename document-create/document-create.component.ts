import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { ErrorPopupService } from '@appCore/services/error-popup/error-popup.service';
import { FormsHelperService } from '@appCore/services/form-helper/form-helper.service';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { ToastsDocumentService } from '@appCore/services/toasts/toasts-document.service';
import { getPopupIsOpened } from '@appCore/store/popup';
import { PopupClose } from '@appCore/store/popup/popup.actions';
import * as fromDocumentPackageStore from '@appDocuments/core/store/document-package';
import { getDocumentPackage } from '@appDocuments/core/store/document-package';
import {
  DocumentPackageReset,
  DocumentPackageSave,
} from '@appDocuments/core/store/document-package/document-package.actions';
import { RegistrationNumberReset } from '@appDocuments/core/store/registration-number/registration-number.actions';
import { CompositionDocumentsCopiesActionReset } from '@appDocuments/documents-composition/store/composition-documents-copies/composition-documents-copies.actions';
import { CompositionDocumentsToPrintActionReset } from '@appDocuments/documents-composition/store/composition-documents-to-print/composition-documents-to-print.actions';
import { CompositionDocumentsActionReset } from '@appDocuments/documents-composition/store/composition-documents/composition-documents.actions';
import { DocumentPageMode } from '@appDocuments/shared/enums/document-page-mode.enum';
import { DocumentsAboutFieldsLabel } from '@appDocuments/shared/enums/documents-about-fields-label.enum';
import { DocumentCreateFormService } from '@appDocuments/shared/services/document-create-form.service';
import { ButtonStyles } from '@appShared/enums/button-styles.enum';
import { ControlStyle } from '@appShared/enums/control-style.enum';
import { DocumentSaveStatuses } from '@appShared/enums/documents-save-statuses.enum';
import { ProjectGroupType } from '@models/enums/project-group-type.enum';
import { Observable, Subject } from 'rxjs';
import { delay, filter, first, map, takeUntil, tap } from 'rxjs/operators';
import { SavePackageMapperService } from './services/save-package-mapper.service';
import { ProjectTypeModel } from '@models/document-package/project-type.model';
import { DocumentsSidebarLogicService } from '@appDocuments/core/components/documents-sidebar/documents-sidebar-logic.service';

/** Компонент создания пакета документов */
@Component({
  selector: 'app-document-create',
  templateUrl: './document-create.component.html',
  styleUrls: ['./document-create.component.scss', '../documents.public.styles.scss'],
})
export class DocumentCreateComponent implements OnInit, OnDestroy {
  isDocumentPackageLoading$: Observable<boolean>;
  isOpenPopup$: Observable<boolean>;
  invalidControlsNames: string[];
  aboutDocumentForm: FormGroup;
  registrationNumber: string;
  npaRegistrationNumber: string;
  toggleLoader: boolean;

  toggleFormValidationErrorPopup = false;
  controlStyle = ControlStyle;
  buttonStyles = ButtonStyles;

  public toggleLeavePopup: boolean;
  public leaveUrl: string;
  public canDeactivate: boolean;
  public documentPackageId: number;
  public documentPageMode = DocumentPageMode;
  public isLocalProjectTypeSelected$: Observable<boolean>;

  private ngUnsubscribe: Subject<void> = new Subject<void>();
  public getProjectTypeData: Subject<void> = new Subject<void>();

  constructor(
    private formsHelperService: FormsHelperService,
    private documentCreateForm: DocumentCreateFormService,
    private savePackageMapper: SavePackageMapperService,
    private redux: ReduxService,
    private router: Router,
    private activateRoute: ActivatedRoute,
    private toastsService: ToastsDocumentService,
    private errorPopup: ErrorPopupService,
    private asideLogic: DocumentsSidebarLogicService
  ) {}

  /** Проверка закрытия страницы браузера*/
  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler() {
    return false;
  }

  ngOnInit() {
    this.isDocumentPackageLoading$ = this.redux
      .selectStore(fromDocumentPackageStore.getIsDocumentPackageLoading)
      .pipe(delay(0));
    this.isOpenPopup$ = this.redux.selectStore(getPopupIsOpened).pipe(delay(0));

    this.redux.dispatchAction(new CompositionDocumentsActionReset());
    this.redux.dispatchAction(new CompositionDocumentsCopiesActionReset());
    this.redux.dispatchAction(new CompositionDocumentsToPrintActionReset());

    this.resetPackageNumber();
    this.initAboutDocumentForm();

    this.subscribeToSaveSuccess();
    this.subscribeToSaveError();
    this.subscribeToRouterNavigationStart();

    this.isLocalProjectTypeSelected$ = this.getSelectedProjectType();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  // Инициирует сброс учётного номера ранее просмотренного ПД из стора
  private resetPackageNumber() {
    this.redux.dispatchAction(new RegistrationNumberReset());
  }

  /** список имен полей, не прошедших валидацию */
  public getInvalidContolsNames(): string[] {
    return [
      ...this.getInvalidControlsNamesInForm(this.mainForm),
      ...this.getInvalidControlsNamesInForm(this.developmentReasonForm),
      ...this.getInvalidControlsNamesInForm(this.reviewDateForm),
      ...this.getInvalidControlsNamesInForm(this.presentationForm),
      ...this.getInvalidControlsNamesInForm(this.representationDateForm),
    ];
  }

  /** Форма с основными данными */
  public get mainForm() {
    return this.aboutDocumentForm.get('main') as FormGroup;
  }
  /** Форма основание разработки*/
  public get developmentReasonForm() {
    return this.aboutDocumentForm.get('developmentReason') as FormGroup;
  }
  /** Форма дат рассмотрения */
  public get reviewDateForm() {
    return this.aboutDocumentForm.get('reviewDate') as FormGroup;
  }
  /** Форма презентационных маетриалов */
  public get presentationForm() {
    return this.aboutDocumentForm.get('presentation') as FormGroup;
  }
  /** Форма пердоставление в ОФГ АМиПМ */
  public get representationDateForm() {
    return this.aboutDocumentForm.get('representationDate') as FormGroup;
  }

  /** Отправка формы */
  public onSubmit() {
    if (this.aboutDocumentForm.invalid) {
      this.invalidControlsNames = [...this.getInvalidContolsNames()];
      this.formsHelperService.markAsTouchedDeep(this.aboutDocumentForm);
      this.toggleFormValidationErrorPopup = true;
      return;
    }

    this.toggleLoader = true;
    const formData = this.savePackageMapper.mapFormToSaveRequest(this.aboutDocumentForm.value, null);

    this.redux.dispatchAction(new DocumentPackageSave(formData));

    this.redux
      .selectStore(getDocumentPackage)
      .pipe(first((documentPackage) => !!documentPackage && !!documentPackage.message))
      .subscribe((documentPackage) => this.errorPopup.show({ message: documentPackage.message }));
  }

  /** При подтверждении ухода со страницы */
  public onPageLeave(): void {
    this.canDeactivate = true;

    if (this.router.url === this.leaveUrl) {
      this.initAboutDocumentForm();
      this.redux.dispatchAction(new PopupClose());
      this.mainForm
        .get('projectType')
        .valueChanges.pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(() => {
          this.getProjectTypeData.next();
        });
    }

    this.router.navigateByUrl(this.leaveUrl).then();
  }

  public onSuccessSavePopupClose(): void {
    this.canDeactivate = true;
    this.router.navigateByUrl(`/documents/edit/${this.documentPackageId}`).then();
  }

  /** Подписка на ошибку сохранения */
  private subscribeToSaveError(): void {
    this.redux
      .selectStore(fromDocumentPackageStore.getDocumentPackageError)
      .pipe(
        filter((err) => err),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe(() => {
        this.toggleLoader = false;
        this.toastsService.addToast({ title: DocumentSaveStatuses.createError });
      });
  }

  /** Подписка на событие старта навигации */
  private subscribeToRouterNavigationStart(): void {
    this.router.events
      .pipe(
        filter((val) => val instanceof NavigationStart),
        filter(() => !this.canDeactivate),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((val: NavigationStart) => {
        this.leaveUrl = val.url;
        this.toggleLeavePopup = true;
      });
  }

  /** Подписка на сохранение */
  private subscribeToSaveSuccess(): void {
    this.redux
      .selectStore(fromDocumentPackageStore.getDocumentPackage)
      .pipe(
        filter((documentPackage) => !!documentPackage),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((documentPackage) => {
        this.documentPackageId = documentPackage.id;
        this.toastsService.addToast({ title: DocumentSaveStatuses.createSuccess });
        this.toggleLoader = false;
        this.onSuccessSavePopupClose();
      });
  }

  /** Получение не валидных контролов из формы */
  private getInvalidControlsNamesInForm(form: FormGroup): string[] {
    const controls = form ? form.controls : [];
    const result: string[] = [];

    for (const name in controls) {
      if (controls[name].invalid) {
        result.push(DocumentsAboutFieldsLabel[name] || name);
      }
    }

    return result;
  }

  /** сбросить форму */
  private initAboutDocumentForm(): void {
    this.redux.dispatchAction(new DocumentPackageReset());
    this.aboutDocumentForm = this.documentCreateForm.createDocumentsAboutForm();
    this.formsHelperService.markAsTouchedDeep(this.aboutDocumentForm);
    setTimeout(() => {
      if (this.aboutDocumentForm) {
        this.aboutDocumentForm.get('packageType').patchValue({
          id: this.activateRoute.snapshot.params['packageType'],
        });
      }
    }, 100);
  }

  private getSelectedProjectType(): Observable<boolean> {
    return this.mainForm.get('projectType').valueChanges.pipe(
      tap((value: ProjectTypeModel) => {
        this.asideLogic.setProjectGroupId(value?.projectGroupType.id);
      }),
      map((projectType) => projectType && projectType.projectGroupType.id === ProjectGroupType.LOCAL_LEGAL_ACTS)
    );
  }
}
