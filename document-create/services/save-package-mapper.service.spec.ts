import { getMockFormValue } from 'test-mock-data/document-create-form-value.mock';
import { SavePackageMapperService } from './save-package-mapper.service';

describe('SavePackageMapperService', () => {
  let service: SavePackageMapperService;

  beforeEach(() => {
    service = new SavePackageMapperService();
  });

  it('should map form value to requesModel', () => {
    const registrationNumber = getMockRegistrationNumber();
    const formValue = getMockFormValue();

    const mapperResult = service.mapFormToSaveRequest(formValue, null, 1, registrationNumber);

    expect(mapperResult).toMatchSnapshot();
  });
});

function getMockRegistrationNumber(): string {
  return '6975-2394-6753';
}
