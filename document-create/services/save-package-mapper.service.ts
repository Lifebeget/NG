import { Injectable } from '@angular/core';
import { DevelopmentReasonModel } from '@models/document-package/development-reasons/development-reason.model';
import { DocumentPackageRequestModel } from '@models/document-package/document-package-request.model';
import { DocumentPackageMainInfo } from '@models/document-package/main-info/document-package-main-info.model';
import { RelatedDocumentsModel } from '@models/document-package/related-documents/related-documents.model';
import { DevelopmentReasonFormModel } from '../../shared/models/development-reason-form.model';
import { DocumentPackageFormModel } from '../../shared/models/document-package-form.model';
import { DocumentPackageMainFormModel } from '../../shared/models/document-package-main-form.model';

@Injectable()
export class SavePackageMapperService {
  constructor() {}

  /** Контракт для сохранения формы */
  public mapFormToSaveRequest(
    formValue: DocumentPackageFormModel,
    relatedDocuments: RelatedDocumentsModel,
    id?: number,
    registrationNumber?: string
  ): DocumentPackageRequestModel {
    const result = {
      id: id || null,
      packageNumber: registrationNumber,
      packageType: formValue.packageType,
      main: this.mapMain(formValue.main),
      developmentReason: this.mapDevelopmentReason(formValue.developmentReason),
      reviewDate: formValue.reviewDate,
      relatedDocuments: {
        affectedDocuments: relatedDocuments ? relatedDocuments.affectedDocuments : [],
        affectingDocuments: relatedDocuments ? relatedDocuments.affectingDocuments : [],
        links: relatedDocuments ? relatedDocuments.links : [],
      },
      categories: [],
    };

    return result;
  }

  /**
   * Мапинг формы "Общие сведения"
   */
  public mapMain(formData: DocumentPackageMainFormModel): DocumentPackageMainInfo {
    return formData;
  }

  /**
   * Мапинг формы "Основание разработки"
   */
  public mapDevelopmentReason(formData: DevelopmentReasonModel): DevelopmentReasonFormModel {
    return {
      comment: formData.comment || null,
      instruction: formData.instruction || null,
      mayorAgreement: this.checkObjectToNotNullableFields(formData.mayorAgreement) ? formData.mayorAgreement : null,
      townPlanningMoscowCommission: formData.townPlanningMoscowCommission || null,
      commissionProtocolNumber: formData.commissionProtocolNumber || null,
      commissionProtocolDate: formData.commissionProtocolDate || null,
    };
  }

  /** проверка на наличие в объекте не пустых значений */
  private checkObjectToNotNullableFields(obj: Object): boolean {
    return obj && Object.values(obj).some((v) => !!v);
  }
}
