import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { DocumentsSharedModule } from '../shared/documents-shared.module';
import { DocumentCreateComponent } from './document-create.component';
import { SavePackageMapperService } from './services/save-package-mapper.service';

@NgModule({
  imports: [CommonModule, SharedModule, ReactiveFormsModule, DocumentsSharedModule],
  declarations: [DocumentCreateComponent],
  providers: [SavePackageMapperService]
})
export class DocumentCreateModule {}
