import { AddresseeAddingPopupComponent } from '../components/popups/addressee-adding-popup/addressee-adding-popup.component';
import { AddresseeEditionPopupComponent } from '../components/popups/addressee-edition-popup/addressee-edition-popup.component';
import { AddresseeRemovePopupComponent } from '../components/popups/addressee-remove-popup/addressee-remove-popup.component';
import { AddresseeRepeatedlySendingPopupComponent } from '../components/popups/addressee-repeatedly-sending-popup/addressee-repeatedly-sending-popup.component';
import { AddresseeSendToAllPopupComponent } from '../components/popups/addressee-send-to-all-popup/addressee-send-to-all-popup.component';
import { AddresseeSendingParamsPopupComponent } from '../components/popups/addressee-sending-params-popup/addressee-sending-params-popup.component';
import { SendingsPopupTypes } from '../enums/sendings-popup-types.enum';

export const sendingsPopupTypeComponentMap = new Map<string, any>([
  [SendingsPopupTypes.addresseeAdding, AddresseeAddingPopupComponent],
  [SendingsPopupTypes.addresseeEdition, AddresseeEditionPopupComponent],
  [SendingsPopupTypes.addresseeRemove, AddresseeRemovePopupComponent],
  [SendingsPopupTypes.addresseeSendToAll, AddresseeSendToAllPopupComponent],
  [SendingsPopupTypes.addresseeSendingParams, AddresseeSendingParamsPopupComponent],
  [SendingsPopupTypes.addresseeRepeatedlySending, AddresseeRepeatedlySendingPopupComponent],
]);
