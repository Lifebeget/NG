export enum SendingStatus {
  UNSENT = 'UNSENT',
  SENT = 'SENT',
  SENT_REPEATEDLY = 'SENT_REPEATEDLY',
}
