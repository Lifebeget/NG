export enum SendingsPopupTypes {
  addresseeAdding = 'addresseeAdding',
  addresseeEdition = 'addresseeEdition',
  addresseeRemove = 'addresseeRemove',
  addresseeSendToAll = 'addresseeSendToAll',
  addresseeSendingParams = 'addresseeSendingParams',
  addresseeRepeatedlySending = 'addresseeRepeatedlySending',
}
