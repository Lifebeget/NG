export enum AddresseeType {
  Organization = 'Организация',
  Personally = 'Лично',
}

export enum AddresseeTypeKeys {
  Organization = 'Organization',
  Personally = 'Personally',
}
