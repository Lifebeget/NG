import { FormGroup, ValidationErrors } from '@angular/forms';

export function emptyFormValidator(form: FormGroup): ValidationErrors | null {
  const formValue = form.value;

  const haveSomeAddressee = formValue.employee || formValue.organization;
  const haveSomeSending = formValue.isEmail || formValue.isPaper || formValue.isEdo;

  if (!haveSomeAddressee || !haveSomeSending) {
    return { formEmpty: true };
  }

  return null;
}
