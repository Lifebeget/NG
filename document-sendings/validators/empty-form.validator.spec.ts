import { FormBuilder, FormGroup } from '@angular/forms';
import { EMPLOYEE_MOCK } from 'test-mock-data/user-info/employee-base-model.mock';
import { AddresseeTypeKeys } from '../enums/addressee-type.enum';
import { emptyFormValidator } from './empty-form.validator';

describe('emptyFormValidator', () => {
  const formBuilder = new FormBuilder();
  let employeeForm: FormGroup;
  let organizationForm: FormGroup;

  beforeEach(() => {
    employeeForm = formBuilder.group({
      addresseeType: AddresseeTypeKeys.Personally,
      employee: null,
      isApprovalSheet: false,
      isPaper: false,
      isEmail: false,
    });

    organizationForm = formBuilder.group({
      addresseeType: AddresseeTypeKeys.Organization,
      organization: null,
      isApprovalSheet: false,
      isPaper: false,
      isEmail: false,
    });
  });

  it('пустую форму считать не валидной', () => {
    expect(emptyFormValidator(employeeForm)).toEqual({ formEmpty: true });
  });

  it('форму с сотрудником и бумажной рассылкой считать валидной', () => {
    employeeForm.controls.employee.setValue(EMPLOYEE_MOCK);
    employeeForm.controls.isPaper.setValue(true);
    expect(emptyFormValidator(employeeForm)).toBeNull();
  });

  it('форму с сотрудником и электронной рассылкой считать валидной', () => {
    employeeForm.controls.employee.setValue(EMPLOYEE_MOCK);
    employeeForm.controls.isEmail.setValue(true);
    expect(emptyFormValidator(employeeForm)).toBeNull();
  });

  it('форму с организацией и бумажной рассылкой считать валидной', () => {
    organizationForm.controls.organization.setValue({
      id: 2,
      name: 'Московский городской транспорт',
      shortName: 'ГУМЧС',
    });
    organizationForm.controls.isPaper.setValue(true);
    expect(emptyFormValidator(organizationForm)).toBeNull();
  });

  it('форму с организацией и электронной рассылкой считать валидной', () => {
    organizationForm.controls.organization.setValue({
      id: 2,
      name: 'Московский городской транспорт',
      shortName: 'ГУМЧС',
    });
    organizationForm.controls.isEmail.setValue(true);
    expect(emptyFormValidator(organizationForm)).toBeNull();
  });
});
