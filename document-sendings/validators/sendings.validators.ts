import { Injectable } from '@angular/core';
import { AsyncValidatorFn, FormControl, ValidationErrors } from '@angular/forms';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { getDocumentPackageId } from '@appDocuments/core/store/document-package';
import { DocumentPackageApiService } from '@appShared/api/document-package/document-package.api.service';
import { EmployeeBaseModel } from '@models/employee/employee-base.model';
import { OrganizationBaseModel } from '@models/employee/organization-base.model';
import { Observable, of, Subject } from 'rxjs';
import { first, map, switchMap, tap } from 'rxjs/operators';
import { Sending } from '../models/sending.model';

@Injectable({
  providedIn: 'root',
})
export class SendingsValidators {

  public changeDetectionSubject: Subject<void> = new Subject<void>();

  constructor(private redux: ReduxService, private documentPackageApi: DocumentPackageApiService) {}

  public hasOrganizationAsyncValidator(organization?: OrganizationBaseModel): AsyncValidatorFn {
    return (control: FormControl): Observable<ValidationErrors> => {
      if (!control.value) {
        return of(null);
      }

      return this.redux.selectStore(getDocumentPackageId).pipe(
        switchMap((documentPackageId) =>
          this.documentPackageApi.getOrganizationDocumentPackageSendings(documentPackageId, control.value.id).pipe(
            map((res: Sending[]) => {
              const result = res[0];

              if (organization) {
                if (result && result.organization && result.organization.id !== organization.id) {
                  return { hasOrganization: 'Адресат уже есть в списке рассылки' };
                }
                return null;
              }

              if (result) {
                return { hasOrganization: 'Адресат уже есть в списке рассылки' };
              }
              return null;
            })
          )
        ),
        tap(() => this.changeDetectionSubject.next()),
        first()
      );
    };
  }

  public hasEmployeeAsyncValidator(employee?: EmployeeBaseModel): AsyncValidatorFn {
    return (control: FormControl): Observable<ValidationErrors> => {
      if (!control.value) {
        return of(null);
      }

      return this.redux.selectStore(getDocumentPackageId).pipe(
        switchMap((documentPackageId) =>
          this.documentPackageApi.getEmployeeDocumentPackageSendings(documentPackageId, control.value.id).pipe(
            map((res: Sending[]) => {
              const result = res[0];
              if (employee) {
                if (result && result.employee && result.employee.id !== employee.id) {
                  return { hasEmployee: 'Адресат уже есть в списке рассылки' };
                }
                return null;
              }

              if (result) {
                return { hasEmployee: 'Адресат уже есть в списке рассылки' };
              }
              return null;
            })
          )
        ),
        tap(() => this.changeDetectionSubject.next()),
        first()
      );
    };
  }
}
