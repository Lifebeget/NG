import { EdoSendingModel } from './edo-sending.model';
import { EmailSendingModel } from './email-sending.model';

export interface EdoWithEmailSendingResponceModel {
  edoSending: EdoSendingModel;
  emailSending: EmailSendingModel;
}
