import { EmployeeBaseModel } from '@models/employee/employee-base.model';
import { SendingStatus } from '../enums/sending-status.enum';

/** ЭДО */
export class EdoSendingModel {
  id: number;
  /** Дата */
  date: string;
  /** Комментарий */
  comment: string;
  /** Статус рассылки */
  status: SendingStatus;
  /** Отправитель */
  sender: EmployeeBaseModel;
}
