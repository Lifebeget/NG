import { SendingMailItemFormModel } from '@appDocuments/document-sendings/models/sending-mail-item-form-value-model';
import { SendingPaperItemFormModel } from '@appDocuments/document-sendings/models/sending-paper-item-form-value-model';
import { DictionaryWithShortNameModel } from '@models/base/dictionary-with-short-name.model';
import { EmployeeBaseModel } from '@models/employee/employee-base.model';

export class SendingFormItemModel {
  /** id */
  id: number;
  /** сотрудник */
  employee: EmployeeBaseModel;
  /** организация */
  organization: DictionaryWithShortNameModel;
  /** признак того, что рассылка личная  */
  isEmployee: boolean;
  /** Признак бумажной рассылки */
  isPaper: boolean;
  /** Признак электронной рассылки */
  isEmail: boolean;
  /** Признак листа согласования */
  isApprovalSheet: boolean;
  /** электронная рассылка */
  emailSending: SendingMailItemFormModel;
  /** бумажная рассылка */
  paperSending: SendingPaperItemFormModel;
}
