import { EmployeeBaseModel } from '@models/employee/employee-base.model';
import { SendingStatus } from '../enums/sending-status.enum';

/** Бумажная рассылка */
export class PaperSendingModel {
  id: number;
  /** Дата */
  date: string;
  /** Количество */
  quantity: number;
  /** Статус рассылки */
  status: SendingStatus;
  /** адрес рассылки */
  address: string;
  /** комментарий */
  comment: string;
  /** Отправитель */
  sender: EmployeeBaseModel;
}
