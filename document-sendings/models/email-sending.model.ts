import { EmployeeBaseModel } from '@models/employee/employee-base.model';
import { SendingStatus } from '../enums/sending-status.enum';

/** Электронная рассылка */
export class EmailSendingModel {
  id: number;
  /** Дата */
  date: string;
  /** e-mail */
  email: string;
  /** Комментарий */
  comment: string;
  /** Статус рассылки */
  status: SendingStatus;
  /** Отправитель */
  sender: EmployeeBaseModel;
}
