export interface RepeatedSendingModel {
  sendingId: number;
  isEdo: boolean;
  isEmail: boolean;
  isPaper: boolean;
  sendingDate: string;
}
