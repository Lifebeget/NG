import { SendingFormItemModel } from '@appDocuments/document-sendings/models/sending-form-item.model';

/** Модель рассылки */
export class SendingFormModel {
  noSending: boolean;
  noSendingComment: string;
  doneSending: boolean;
  sendings: SendingFormItemModel[];
}
