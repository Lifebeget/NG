import { Sending } from './sending.model';

export class SendingsLazyModel {
  content: Sending[];
  currentPageNumber: number;
  pageCount: number;
  totalResultsCount: number;
}
