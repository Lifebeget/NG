import { EmailSendingModel } from '@appDocuments/document-sendings/models/email-sending.model';
import { PaperSendingModel } from '@appDocuments/document-sendings/models/paper-sending.model';
import { DictionaryWithShortNameModel } from '@models/base/dictionary-with-short-name.model';
import { EmployeeBaseModel } from '@models/employee/employee-base.model';
import { EdoSendingModel } from './edo-sending.model';

export class Sending {
  /** id */
  id: number;
  /** сотрудник */
  employee: EmployeeBaseModel;
  /** организация */
  organization: DictionaryWithShortNameModel;
  /** Признак ЭДО */
  isEdo: boolean;
  /** Признак бумажной рассылки */
  isPaper: boolean;
  /** Признак электронной рассылки */
  isEmail: boolean;
  /** Признак листа согласования */
  isApprovalSheet: boolean;
  /** ЭДО */
  edoSending: EdoSendingModel;
  /** электронная рассылка */
  emailSending: EmailSendingModel;
  /** бумажная рассылка */
  paperSending: PaperSendingModel;
}
