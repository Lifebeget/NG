import { DictionaryWithShortNameModel } from '@models/base/dictionary-with-short-name.model';
import { EmployeeBaseModel } from '@models/employee/employee-base.model';
import { SendingParamsFormType } from '../enums/sending-params-form-type.enum';
import { EmailSendingModel } from './email-sending.model';
import { PaperSendingModel } from './paper-sending.model';

export interface SendingParamsModel {
  id: number;
  type: SendingParamsFormType;
  employee: EmployeeBaseModel;
  organization: DictionaryWithShortNameModel;
  emailSending: EmailSendingModel;
  paperSending: PaperSendingModel;
  sender: EmployeeBaseModel;
}
