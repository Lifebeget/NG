export class PaperSendingRequestModel {
  id: number;
  date: string;
  quantity: number;
}
