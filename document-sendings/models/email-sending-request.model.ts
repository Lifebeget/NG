export class EmailSendingRequestModel {
  sendingId: number;
  date: string;
}
