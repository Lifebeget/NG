import { Sending } from './sending.model';

export class Sendings {
  public sendings: Sending[];
  public needSending: boolean;
  public noSendingComment: string;
  public doneSending: boolean;
}
