export class MassSendingRequestModel {
  documentPackageId: number;
  isEdo: boolean;
  isEmail: boolean;
  isPaper: boolean;
  sendingDate: string;
}
