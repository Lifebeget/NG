/** Бумажная рассылка */
export class SendingPaperItemFormModel {
  id: number;
  /** Дата */
  date: string;
  /** Время */
  time: string;
  /** Количество */
  quantity: number;
  /** Признак фактического рассылки */
  fact: boolean;
  /** адрес рассылки */
  address: string;
  /** комментарий */
  comment: string;
}
