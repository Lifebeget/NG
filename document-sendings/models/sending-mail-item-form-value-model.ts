
/** Электронная рассылка */
export class SendingMailItemFormModel {
  id: number;
  /** Дата */
  date: string;
  /** Время */
  time: string;
  /** e-mail */
  email: string;
  /** Комментарий */
  comment: string;
  /** Признак фактического опубликования */
  fact: boolean;
  /** Автоматическая рассылка */
  autoSending: boolean;
}
