export interface SendtToAllParamsModel {
  documentPackageId: number;
  employeeId: number;
  organizationId: number;
}
