export interface UnsentSendingsModel {
  countEdo: number;
  countEmail: number;
  countPaper: number;
}
