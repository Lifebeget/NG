import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SendingsServicesModule } from '@appDocuments/document-sendings/services/sendings-services.module';
import { DocumentsSharedModule } from '@appDocuments/shared/documents-shared.module';
import { SharedModule } from '@appShared/shared.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AddresseeCardComponent } from './components/addressee-card/addressee-card.component';
import { AddresseeAddingPopupComponent } from './components/popups/addressee-adding-popup/addressee-adding-popup.component';
import { AddresseeEditionPopupComponent } from './components/popups/addressee-edition-popup/addressee-edition-popup.component';
import { AddresseePopupBaseComponent } from './components/popups/addressee-popup-base/addressee-popup-base.component';
import { AddresseeRemovePopupComponent } from './components/popups/addressee-remove-popup/addressee-remove-popup.component';
import { AddresseeRepeatedlySendingPopupComponent } from './components/popups/addressee-repeatedly-sending-popup/addressee-repeatedly-sending-popup.component';
import { AddresseeSendToAllPopupComponent } from './components/popups/addressee-send-to-all-popup/addressee-send-to-all-popup.component';
import { AddresseeSendingParamsPopupComponent } from './components/popups/addressee-sending-params-popup/addressee-sending-params-popup.component';
import { SendingParamsFormComponent } from './components/sending-params-form/sending-params-form.component';
import { DocumentSendingsComponent } from './document-sendings.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    DocumentsSharedModule,
    SendingsServicesModule,
    FormsModule,
    InfiniteScrollModule,
  ],
  entryComponents: [
    AddresseeAddingPopupComponent,
    AddresseeEditionPopupComponent,
    AddresseeRemovePopupComponent,
    AddresseeSendToAllPopupComponent,
    AddresseeSendingParamsPopupComponent,
    AddresseeRepeatedlySendingPopupComponent,
  ],
  declarations: [
    DocumentSendingsComponent,
    AddresseeCardComponent,
    AddresseeAddingPopupComponent,
    AddresseeEditionPopupComponent,
    AddresseeRemovePopupComponent,
    AddresseeSendToAllPopupComponent,
    AddresseeSendingParamsPopupComponent,
    SendingParamsFormComponent,
    AddresseePopupBaseComponent,
    AddresseeRepeatedlySendingPopupComponent,
  ],
})
export class DocumentSendingsModule {}
