import { TestBed } from '@angular/core/testing';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { getDocumentPackageId } from '@appDocuments/core/store/document-package';
import { PermissionType } from '@appPermissions/enums/permission-type.enum';
import { SendingPermissions } from '@appPermissions/enums/sending-permissions.enum';
import { DictionaryApiService } from '@appShared/api/dictionary/dictionary.api.service';
import { DictionaryWithShortNameModel } from '@models/base/dictionary-with-short-name.model';
import { of } from 'rxjs';
import { EMPLOYEE_MOCK } from 'test-mock-data/user-info/employee-base-model.mock';
import { instance, mock, when } from 'ts-mockito';
import { SendingParamsFormType } from '../enums/sending-params-form-type.enum';
import { SendingStatus } from '../enums/sending-status.enum';
import { EmailSendingModel } from '../models/email-sending.model';
import { PaperSendingModel } from '../models/paper-sending.model';
import { SendingParamsModel } from '../models/sending-params.model';
import { DocumentSendingsService } from './document-sendings.service';
import { EdoSendingModel } from '@appDocuments/document-sendings/models/edo-sending.model';

describe('DocumentSendingsService', () => {
  let service: DocumentSendingsService;
  let dictionaryApiMock: DictionaryApiService;
  let reduxMock: ReduxService;

  let organizations: DictionaryWithShortNameModel[];
  let sendingParams: SendingParamsModel;
  let emailSending: EmailSendingModel;
  let paperSending: PaperSendingModel;
  let edoSending: EdoSendingModel;

  beforeEach(() => {
    dictionaryApiMock = mock(DictionaryApiService);
    reduxMock = mock(ReduxService);

    TestBed.configureTestingModule({
      providers: [
        DocumentSendingsService,
        { provide: DictionaryApiService, useFactory: () => instance(dictionaryApiMock) },
        { provide: ReduxService, useFactory: () => instance(reduxMock) },
      ],
    });

    service = TestBed.get(DocumentSendingsService);

    organizations = [
      {
        id: 1,
        name: 'Военный комиссариат города Москвы (Военкомат г.Москвы)',
        shortName: 'ВКГМ',
      },
      {
        id: 2,
        name: 'Московский городской транспорт',
        shortName: 'ГУМЧС',
      },
    ];

    emailSending = {
      id: 1,
      date: '2018-04-04T00:00:00+03:00',
      email: 'test@mail',
      comment: '',
      status: SendingStatus.SENT,
      sender: EMPLOYEE_MOCK,
    };

    paperSending = {
      id: 1,
      date: '2018-04-04T00:00:00+03:00',
      quantity: 1,
      address: 'Krasnaya Str.',
      comment: '',
      status: SendingStatus.SENT,
      sender: EMPLOYEE_MOCK,
    };

    sendingParams = {
      id: 1,
      type: SendingParamsFormType.email,
      employee: EMPLOYEE_MOCK,
      organization: null,
      emailSending,
      paperSending: null,
      sender: EMPLOYEE_MOCK,
    };
    edoSending = {
      id: 1,
      date: '2018-04-04T00:00:00+03:00',
      comment: '',
      status: SendingStatus.SENT,
      sender: EMPLOYEE_MOCK,
    };
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('получен список организаций', (done) => {
    when(dictionaryApiMock.getOrganizations('', true)).thenReturn(of(organizations));

    service.getOrganizationsList(true).subscribe((result) => {
      expect(result).toEqual(organizations);
      done();
    });
  });

  it('полученый список организаций отсортирован по имени', (done) => {
    const organizationRevert = organizations.slice().reverse();
    when(dictionaryApiMock.getOrganizations('', true)).thenReturn(of(organizationRevert));

    service.getOrganizationsList(true).subscribe((result) => {
      expect(result).toEqual(organizations);
      done();
    });
  });

  it('получен список пермишенов', (done) => {
    when(reduxMock.selectStore(getDocumentPackageId)).thenReturn(of(1));

    service.getUserPermissions().subscribe((result) => {
      expect(result).toEqual({
        id: 1,
        permissions: SendingPermissions.SENDING_SEND,
        type: PermissionType.sending,
      });
      done();
    });
  });

  describe('Заголовок попапа отправки рассылки', () => {
    it('заголовок электронной рассылки сотруднику с указанным почтовым ящиком', () => {
      const result = service.getTitle(sendingParams);
      expect(result).toBe('Выполнить рассылку по ЭДО и электронную рассылку для Иванов И.И. (test@mail)?');
    });

    it('заголовок электронной рассылки организации', () => {
      sendingParams.employee = null;
      sendingParams.organization = organizations[0];
      const result = service.getTitle(sendingParams);
      expect(result).toBe('Выполнить рассылку по ЭДО и электронную рассылку для ВКГМ (test@mail)?');
    });

    it('заголовок бумажной рассылки сотруднику', () => {
      sendingParams.type = SendingParamsFormType.paper;
      const result = service.getTitle(sendingParams);
      expect(result).toBe('Выполнить бумажную рассылку для Иванов И.И.?');
    });

    it('заголовок бумажной рассылки организации', () => {
      sendingParams.type = SendingParamsFormType.paper;
      sendingParams.employee = null;
      sendingParams.organization = organizations[0];
      const result = service.getTitle(sendingParams);
      expect(result).toBe('Выполнить бумажную рассылку для ВКГМ?');
    });
  });

  describe('Заголовок попапа повторной отправки рассылки', () => {
    it('заголовок электронной рассылки сотруднику без почтового ящика', () => {
      const result = service.getTitleRepeatedlySending(sendingParams);
      expect(result).toBe('Указать, что документы были отправлены повторно для Иванов И.И. (test@mail)?');
    });

    it('заголовок электронной рассылки сотруднику с указанным почтовым ящиком', () => {
      const result = service.getTitleRepeatedlySending(sendingParams);
      expect(result).toBe('Указать, что документы были отправлены повторно для Иванов И.И. (test@mail)?');
    });

    it('заголовок электронной рассылки организации', () => {
      sendingParams.employee = null;
      sendingParams.organization = organizations[0];
      const result = service.getTitleRepeatedlySending(sendingParams);
      expect(result).toBe('Указать, что документы были отправлены повторно для ВКГМ (test@mail)?');
    });
  });

  describe('Статус отправки рассылок', () => {
    it('отправлены и электронная и бумажная и ЭДО рассылки', () => {
      const result = service.wasFullySent(emailSending, paperSending, edoSending);
      expect(result).toBeTruthy();
    });

    it('отправлены повторно и электронная и бумажная и ЭДО рассылки', () => {
      emailSending = { ...emailSending, status: SendingStatus.SENT_REPEATEDLY };
      paperSending = { ...paperSending, status: SendingStatus.SENT_REPEATEDLY };
      edoSending = { ...edoSending, status: SendingStatus.SENT_REPEATEDLY };

      const result = service.wasFullySent(emailSending, paperSending, edoSending);
      expect(result).toBeTruthy();
    });

    it('неотправлены ни электронная ни бумажная ни ЭДО рассылки', () => {
      emailSending = { ...emailSending, status: SendingStatus.UNSENT };
      paperSending = { ...paperSending, status: SendingStatus.UNSENT };
      edoSending = { ...edoSending, status: SendingStatus.UNSENT };

      const result = service.wasFullySent(emailSending, paperSending, edoSending);
      expect(result).toBeFalsy();
    });

    it('электронная и ЭДО рассылки при отсутствии бумажной отправлена ', () => {
      const result = service.wasFullySent(emailSending, null, edoSending);
      expect(result).toBeTruthy();
    });

    it('электронная и ЭДО рассылки при отсутствии бумажной отправлена повторно ', () => {
      emailSending = { ...emailSending, status: SendingStatus.SENT_REPEATEDLY };
      edoSending = { ...edoSending, status: SendingStatus.SENT_REPEATEDLY };
      const result = service.wasFullySent(emailSending, null, edoSending);
      expect(result).toBeTruthy();
    });

    it('электронная и ЭДО рассылки при отсутствии бумажной не отправлена', () => {
      emailSending = { ...emailSending, status: SendingStatus.UNSENT };
      edoSending = { ...edoSending, status: SendingStatus.UNSENT };
      const result = service.wasFullySent(emailSending, null, edoSending);
      expect(result).toBeFalsy();
    });

    it('бумажная и ЭДО рассылки при отсутствии электронной отправлена', () => {
      const result = service.wasFullySent(null, paperSending, edoSending);
      expect(result).toBeTruthy();
    });

    it('бумажная и ЭДО рассылки  при отсутствии электронной отправлена повторно', () => {
      paperSending = { ...paperSending, status: SendingStatus.SENT_REPEATEDLY };
      edoSending = { ...edoSending, status: SendingStatus.SENT_REPEATEDLY };
      const result = service.wasFullySent(null, paperSending, edoSending);
      expect(result).toBeTruthy();
    });

    it('бумажная и ЭДО рассылки при отсутствии электронной не отправлена', () => {
      paperSending = { ...paperSending, status: SendingStatus.UNSENT };
      edoSending = { ...edoSending, status: SendingStatus.UNSENT };
      const result = service.wasFullySent(null, paperSending, edoSending);
      expect(result).toBeFalsy();
    });

    it('ЭДО отправлена', () => {
      const result = service.wasFullySent(null, null, edoSending);
      expect(result).toBeTruthy();
    });

    it('ЭДО отправлена повторно', () => {
      edoSending = { ...emailSending, status: SendingStatus.SENT_REPEATEDLY };
      const result = service.wasFullySent(null, null, edoSending);
      expect(result).toBeTruthy();
    });

    it('ЭДО  не отправлена', () => {
      edoSending = { ...edoSending, status: SendingStatus.UNSENT };
      const result = service.wasFullySent(null, null, edoSending);
      expect(result).toBeFalsy();
    });

    xit('при отсутствии электронной и бумажной и ЭДО рассылок нет статуса об отправке', () => {
      const result = service.wasFullySent(null, null, null);
      expect(result).toBeFalsy();
    });
  });
});
