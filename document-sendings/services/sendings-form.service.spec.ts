import { TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { of } from 'rxjs';
import { getSendingMock } from 'test-mock-data/sending/sending.mock';
import { anything, instance, mock, when } from 'ts-mockito';
import { SendingsFormService } from './sendings-form.service';

describe('SendingsFormService', () => {
  let service: SendingsFormService;
  let reduxMock: ReduxService;

  beforeEach(() => {
    reduxMock = mock(ReduxService);
    TestBed.configureTestingModule({
      providers: [
        SendingsFormService,
        FormBuilder,
        {
          provide: ReduxService,
          useFactory: () => instance(reduxMock),
        },
      ],
    });

    service = TestBed.get(SendingsFormService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  it('должна быть форма для электронной рассылки с данными по умолчанию', () => {
    const form = service.initSendingParamsForm(false);

    expect(form.get('date').value).toBeTruthy();
    expect(form.get('time').value).toBeTruthy();
    expect(form.get('quantity')).toBeNull();
  });

  it('должна быть форма для бумажной рассылки с данными по умолчанию включая ', () => {
    when(reduxMock.selectStore(anything())).thenReturn(of(getSendingMock()));

    const form = service.initSendingParamsForm(true, 1);

    expect(form.get('date').value).toBeTruthy();
    expect(form.get('time').value).toBeTruthy();
    expect(form.get('quantity').value).toEqual(1);
  });
});
