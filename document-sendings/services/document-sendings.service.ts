import { Injectable } from '@angular/core';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { getDocumentPackageId } from '@appDocuments/core/store/document-package';
import { EdoSendingModel } from '@appDocuments/document-sendings/models/edo-sending.model';
import { PermissionType } from '@appPermissions/enums/permission-type.enum';
import { SendingPermissions } from '@appPermissions/enums/sending-permissions.enum';
import { PermissionModel } from '@appPermissions/models/permission.model';
import { DictionaryApiService } from '@appShared/api/dictionary/dictionary.api.service';
import { employeeShortName } from '@appShared/pipes/employee-short-name.pipe';
import { DictionaryModel } from '@models/base/dictionary.model';
import { Observable } from 'rxjs';
import { filter, first, map } from 'rxjs/operators';
import { SendingParamsFormType } from '../enums/sending-params-form-type.enum';
import { SendingStatus } from '../enums/sending-status.enum';
import { EmailSendingModel } from '../models/email-sending.model';
import { PaperSendingModel } from '../models/paper-sending.model';
import { SendingParamsModel } from '../models/sending-params.model';
import { SendingsServicesModule } from './sendings-services.module';

@Injectable({
  providedIn: SendingsServicesModule,
})
export class DocumentSendingsService {
  constructor(private dictionaryApi: DictionaryApiService, private redux: ReduxService) {}

  /**
   * Получить список организаций
   */
  public getOrganizationsList(withEmpty?: boolean): Observable<DictionaryModel[]> {
    return this.dictionaryApi.getOrganizations('', withEmpty).pipe(
      map((organizationList) =>
        organizationList.sort((a, b) => {
          return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
        })
      )
    );
  }

  /**
   * Метода проверяющий отправлены ли бумажная и электронная рассылки
   */
  public wasFullySent(
    emailSending: EmailSendingModel,
    paperSending: PaperSendingModel,
    edoSending: EdoSendingModel
  ): boolean {
    const sended = [SendingStatus.SENT, SendingStatus.SENT_REPEATEDLY];
    const arraySending = [emailSending, paperSending, edoSending];
    return arraySending.filter((value) => value !== null).every((sending) => sended.includes(sending.status));
  }

  public getTitle(payload: SendingParamsModel): string {
    if (payload.type === SendingParamsFormType.paper) {
      if (payload.employee) {
        return `Выполнить бумажную рассылку для ${employeeShortName(payload.employee)}?`;
      }

      if (payload.organization) {
        return `Выполнить бумажную рассылку для ${payload.organization.shortName}?`;
      }
    }

    if (payload.employee) {
      let addressee = `${employeeShortName(payload.employee)}`;
      if (payload.emailSending && payload.emailSending.email) {
        addressee += ` (${payload.emailSending.email})`;
      } else if (payload.employee && payload.employee.email) {
        addressee += ` (${payload.employee.email})`;
      }
      return `Выполнить рассылку по ЭДО и электронную рассылку для ${addressee}?`;
    }
    if (!payload.emailSending) {
      return `Выполнить рассылку по ЭДО и электронную рассылку для ${payload.organization.shortName}?`;
    }

    return `Выполнить рассылку по ЭДО и электронную рассылку для ${payload.organization.shortName} (${payload.emailSending.email})?`;
  }

  public getTitleRepeatedlySending(payload: SendingParamsModel): string {
    if (payload.employee) {
      let addressee = `${employeeShortName(payload.employee)}`;
      if (payload.emailSending && payload.emailSending.email) {
        addressee += ` (${payload.emailSending.email})`;
      } else if (payload.employee.email) {
        addressee += ` (${payload.employee.email})`;
      }
      return `Указать, что документы были отправлены повторно для ${addressee}?`;
    }
    if (!payload.emailSending) {
      return `Указать, что документы были отправлены повторно для ${payload.organization.shortName}?`;
    }

    return `Указать, что документы были отправлены повторно для ${payload.organization.shortName} (${payload.emailSending.email})?`;
  }

  public getUserPermissions(): Observable<PermissionModel> {
    return this.redux.selectStore(getDocumentPackageId).pipe(
      filter((i) => !!i),
      first(),
      map((documentPackageId) => ({
        permissions: SendingPermissions.SENDING_SEND,
        type: PermissionType.sending,
        id: documentPackageId,
      }))
    );
  }
}
