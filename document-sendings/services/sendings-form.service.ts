import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { getSendingById } from '@appDocuments/core/store/document-package-sendings';
import { SendingsServicesModule } from '@appDocuments/document-sendings/services/sendings-services.module';
import moment from 'moment';
import { first } from 'rxjs/operators';
import { AddresseeTypeKeys } from '../enums/addressee-type.enum';
import { Sending } from '../models/sending.model';

@Injectable({ providedIn: SendingsServicesModule })
export class SendingsFormService {
  constructor(private formBuilder: FormBuilder, private redux: ReduxService) {}

  /**
   * Создание формы отправки электронной/бумажной рассылки
   * @param isPaper - флаг определяющий прежназначени ли форма для бумажной рассылки
   * @param sendingId - идентификатор рассылки
   * @returns форма с данными по умолчанию
   */
  public initSendingParamsForm(isPaper?: boolean, sendingId?: number): FormGroup {
    const currentDateTime = moment();

    const formGroup = this.formBuilder.group({
      date: [currentDateTime, [Validators.required]],
      time: [currentDateTime.format('HH:mm'), [Validators.required]],
    });

    if (isPaper) {
      formGroup.addControl('quantity', this.formBuilder.control(1, [Validators.required, Validators.min(1)]));

      this.redux
        .selectStore(getSendingById(sendingId))
        .pipe(first())
        .subscribe((sending) => formGroup.controls.quantity.setValue(sending.paperSending.quantity));
    }

    return formGroup;
  }

  /** Создание формы для массовой и повторной отправки рассылки */
  public initMassSendForm(): FormGroup {
    return this.formBuilder.group({
      isEdo: false,
      isEmail: false,
      isPaper: false,
      sending: this.initSendingParamsForm(false),
    });
  }

  /**
   * Заполнение формы данными при редактировании
   * @param sending - объект с данными для заполнения
   * @param form - форма для заполнения
   */
  public fillEditionForm(sending: Sending, form: FormGroup): void {
    if (sending.employee) {
      form.patchValue({
        addresseeType: AddresseeTypeKeys.Personally,
        employee: sending.employee,
      });
    }

    if (sending.organization) {
      form.patchValue({
        addresseeType: AddresseeTypeKeys.Organization,
        organization: sending.organization,
      });
    }

    form.patchValue({
      isApprovalSheet: sending.isApprovalSheet,
      isEdo: sending.isEdo,
      isPaper: sending.isPaper,
      isEmail: sending.isEmail,
    });

    if (sending.edoSending) {
      form.patchValue({
        edoSending: sending.edoSending,
      });
    }

    if (sending.emailSending) {
      form.patchValue({
        emailSending: sending.emailSending,
      });
    }

    if (sending.paperSending) {
      form.patchValue({
        paperSending: sending.paperSending,
      });
    }
  }
}
