import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Sending } from '@appDocuments/document-sendings/models/sending.model';
import { SendingsServicesModule } from '@appDocuments/document-sendings/services/sendings-services.module';
import moment from 'moment';
import { AddresseeTypeKeys } from '../enums/addressee-type.enum';
import { SendingStatus } from '../enums/sending-status.enum';
import { EdoSendingModel } from '../models/edo-sending.model';
import { EmailSendingRequestModel } from '../models/email-sending-request.model';
import { EmailSendingModel } from '../models/email-sending.model';
import { MassSendingRequestModel } from '../models/mass-sending-request.model';
import { PaperSendingRequestModel } from '../models/paper-sending-request.model';
import { PaperSendingModel } from '../models/paper-sending.model';

@Injectable({ providedIn: SendingsServicesModule })
export class SendingsMapperService {
  /**
   * Маппинг новой рассылки
   * @param sendingForm - форма с данными для маппинга
   * @returns модель адресата рассылки
   */
  public mapAddingSendingForm(sendingForm: FormGroup): Sending {
    const sendingFormModel = sendingForm.getRawValue() as Sending;

    const edoSending: EdoSendingModel = this.getEdoSendingModel();
    edoSending.comment =
      sendingFormModel.isEdo && sendingFormModel.edoSending.comment ? sendingFormModel.edoSending.comment : null;

    const emailSending: EmailSendingModel = this.getEmailSendingModel();
    emailSending.email = sendingFormModel.isEmail ? sendingFormModel.emailSending.email : null;
    emailSending.comment =
      sendingFormModel.isEmail && sendingFormModel.emailSending.comment ? sendingFormModel.emailSending.comment : null;

    const paperSending: PaperSendingModel = this.getPaperSendingModel();
    paperSending.address = sendingFormModel.isPaper ? sendingFormModel.paperSending.address : null;
    paperSending.comment =
      sendingFormModel.isPaper && sendingFormModel.paperSending.comment ? sendingFormModel.paperSending.comment : null;
    paperSending.quantity =
      sendingFormModel.isPaper && sendingFormModel.paperSending.quantity ? sendingFormModel.paperSending.quantity : 1;

    return {
      id: null,
      employee: sendingFormModel.employee ? sendingFormModel.employee : null,
      organization: sendingFormModel.organization ? sendingFormModel.organization : null,
      isEdo: sendingFormModel.isEdo,
      isPaper: sendingFormModel.isPaper,
      isEmail: sendingFormModel.isEmail,
      isApprovalSheet: sendingFormModel.isApprovalSheet,
      edoSending: edoSending,
      emailSending: emailSending,
      paperSending: paperSending,
    };
  }

  /**
   * Маппинг отредактированной рассылки
   * @param sendingForm - форма с данными для маппинга
   * @returns модель адресата рассылки
   */
  public mapEditionSendingForm(sendingForm: FormGroup, sending: Sending): Sending {
    const sendingFormModel = sendingForm.getRawValue();

    const editedSending = {
      id: sending.id,
      employee: null,
      organization: null,
      isApprovalSheet: sendingFormModel.isApprovalSheet,
      isEdo: sendingFormModel.isEdo,
      isEmail: sendingFormModel.isEmail,
      isPaper: sendingFormModel.isPaper,
      edoSending: null,
      emailSending: null,
      paperSending: null,
    };

    if (sendingFormModel.employee && sendingFormModel.addresseeType === AddresseeTypeKeys.Personally) {
      editedSending.employee = sendingFormModel.employee;
    }

    if (sendingFormModel.organization && sendingFormModel.addresseeType === AddresseeTypeKeys.Organization) {
      editedSending.organization = sendingFormModel.organization;
    }

    if (sendingFormModel.isEdo) {
      editedSending.edoSending = sendingFormModel.edoSending;
    }

    if (sendingFormModel.isEmail) {
      editedSending.emailSending = sendingFormModel.emailSending;
    }

    if (sendingFormModel.isPaper) {
      editedSending.paperSending = sendingFormModel.paperSending;
    }

    return editedSending;
  }

  /**
   * Маппинг параметров для отправки электронной рассылки
   * @param form - форма с данными
   * @param id - идентификатор электронной рассыки (не карточки адресата, а именно его рассылки)
   * @returns смапленная модель
   */
  public mapEmailSendingParamsForm(form: FormGroup, sendingId: number): EmailSendingRequestModel {
    const formData = form.getRawValue();
    return {
      sendingId,
      date: this.mapDatetimeSending(formData.date, formData.time),
    };
  }

  /**
   * Маппинг параметров для отправки бумажной рассылки
   * @param form - форма с данными
   * @param id - идентификатор бумажной рассыки (не карточки адресата, а именно его рассылки)
   * @returns смапленная модель
   */
  public mapPaperSendingParamsForm(form: FormGroup, id: number): PaperSendingRequestModel {
    const formData = form.getRawValue();
    return {
      id: id,
      date: this.mapDatetimeSending(formData.date, formData.time),
      quantity: formData.quantity,
    };
  }

  /**
   * Маппинг параметров для массовой отправки всех доступных рассылок в ПД
   */
  public mapMassSendignParamsForm(form: FormGroup, documentPackageId: number): MassSendingRequestModel {
    const formData = form.getRawValue();
    const request = {
      documentPackageId,
      isEdo: formData.isEdo,
      isEmail: formData.isEmail,
      isPaper: formData.isPaper,
      sendingDate: this.mapDatetimeSending(formData.sending.date, formData.sending.time),
    };
    return request;
  }

  private getEdoSendingModel(): EdoSendingModel {
    return {
      id: null,
      date: null,
      comment: '',
      status: SendingStatus.UNSENT,
      sender: null,
    };
  }

  private getEmailSendingModel(): EmailSendingModel {
    return {
      id: null,
      date: null,
      email: null,
      comment: '',
      status: SendingStatus.UNSENT,
      sender: null,
    };
  }

  private getPaperSendingModel(): PaperSendingModel {
    return {
      id: null,
      date: null,
      address: null,
      comment: '',
      status: SendingStatus.UNSENT,
      quantity: 1,
      sender: null,
    };
  }

  /**
   * Маппинг даты и времени в единое значение
   */
  public mapDatetimeSending(date: string, time: string): string {
    const timeData = moment(time, 'H:m');
    const hours = timeData.format('H');
    const minutes = timeData.format('m');
    return moment(date).add(hours, 'hours').add(minutes, 'minutes').format();
  }
}
