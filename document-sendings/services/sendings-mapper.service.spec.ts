import { TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { getSendingMock } from 'test-mock-data/sending/sending.mock';
import { AddresseeTypeKeys } from '../enums/addressee-type.enum';
import { SendingsMapperService } from './sendings-mapper.service';

describe('SendingsMapperService', () => {
  let service: SendingsMapperService;
  let formBuilder: FormBuilder;

  const sendingMockRequest = getSendingMock();

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormBuilder, SendingsMapperService],
    });
    service = TestBed.get(SendingsMapperService);
    formBuilder = TestBed.get(FormBuilder);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  describe('Маппинг формы редактирования', () => {
    describe('Маппинг поля сотрудника', () => {
      it('если меняется поле сотрудника, исходная модель обновляется', () => {
        const employee = {
          id: 2,
          login: 'ivanov',
          firstName: 'Петр',
          lastName: 'Петров',
          patronymic: 'Петрвич',
          email: null,
          phone: null,
          jobPosition: {
            id: 2,
            name: 'Ведущий специалист',
          },
          organization: {
            id: 2,
            name: 'Главное архивное управление города Москвы (Главархив)',
            shortName: 'ГАУГМ',
          },
          photoUrl: null,
          gender: null,
          oibId: null,
          employeeRevision: null,
        };
        const formGroup = formBuilder.group({
          addresseeType: AddresseeTypeKeys.Personally,
          employee: employee,
        });

        const result = service.mapEditionSendingForm(formGroup, sendingMockRequest);

        expect(result.employee).toEqual(employee);
      });
    });

    describe('Маппинг поля организации', () => {
      it('если меняется поле организации, исходная модель обновляется', () => {
        const organization = {
          id: 2,
          name: 'Московский городской транспорт',
          shortName: 'ГУМЧС',
        };
        const formGroup = formBuilder.group({
          addresseeType: AddresseeTypeKeys.Organization,
          organization: organization,
        });

        const result = service.mapEditionSendingForm(formGroup, sendingMockRequest);

        expect(result.organization).toEqual(organization);
      });
    });

    describe('Маппинг ЭДО', () => {
      it('непустой комментарий в ЭДО, обновляет исходную модель ', () => {
        const comment = 'комментарий';
        const formGroup = formBuilder.group({
          isEdo: true,
          edoSending: formBuilder.group({
            comment,
          }),
        });

        const result = service.mapEditionSendingForm(formGroup, sendingMockRequest);

        expect(result.edoSending.comment).toEqual(comment);
      });
    });

    describe('Маппинг электронной рассылки', () => {
      it('если изменилась почта в электронной рассылке, исходная модель обновляется', () => {
        const email = 'anothertest@mail.com';
        const formGroup = formBuilder.group({
          isEmail: true,
          emailSending: formBuilder.group({
            email,
          }),
        });

        const result = service.mapEditionSendingForm(formGroup, sendingMockRequest);

        expect(result.emailSending.email).toEqual(email);
      });

      it('непустой комментарий в электронной рассылке, обновляет исходную модель ', () => {
        const comment = 'комментарий';
        const formGroup = formBuilder.group({
          isEmail: true,
          emailSending: formBuilder.group({
            comment,
          }),
        });

        const result = service.mapEditionSendingForm(formGroup, sendingMockRequest);

        expect(result.emailSending.comment).toEqual(comment);
      });
    });

    describe('Маппинг бумажной рассылки', () => {
      it('если изменился адрес в бумажной рассылке, исходная модель обновляется', () => {
        const address = 'Сталинградская 227';
        const formGroup = formBuilder.group({
          isPaper: true,
          paperSending: formBuilder.group({
            address,
          }),
        });

        const result = service.mapEditionSendingForm(formGroup, sendingMockRequest);

        expect(result.paperSending.address).toEqual(address);
      });

      it('если изменилось кол-во копий в бумажной рассылке, исходная модель обновляется', () => {
        const quantity = 3;
        const formGroup = formBuilder.group({
          isPaper: true,
          paperSending: formBuilder.group({
            quantity,
          }),
        });

        const result = service.mapEditionSendingForm(formGroup, sendingMockRequest);
        expect(result.paperSending.quantity).toEqual(quantity);
      });

      it('непустой комментарий в бумажной рассылке, обновляет исходную модель ', () => {
        const comment = 'комментарий';
        const formGroup = formBuilder.group({
          isPaper: true,
          paperSending: formBuilder.group({
            comment,
          }),
        });

        const result = service.mapEditionSendingForm(formGroup, sendingMockRequest);
        expect(result.paperSending.comment).toEqual(comment);
      });
    });
  });
});
