import { AfterViewInit, Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { ToastsDocumentService } from '@appCore/services/toasts/toasts-document.service';
import { getRolesForCurrentUser } from '@appCore/store/user-info';
import {
  getSendings,
  getSendingsPopupPayload,
  getSendingsPopupType,
  getSendingsStatus,
  getSendingsTotalCount,
} from '@appDocuments/core/store/document-package-sendings';
import {
  DocumentPackageSendingsGet,
  DocumentPackageSendingsGetSearch,
  DocumentPackageSendingsResetSendings,
  SetSendingsPopupType,
} from '@appDocuments/core/store/document-package-sendings/document-package-sendings.actions';
import { AddresseeAddingPopupComponent } from '@appDocuments/document-sendings/components/popups/addressee-adding-popup/addressee-adding-popup.component';
import { PermissionModel } from '@appPermissions/models/permission.model';
import { AdDirective } from '@appShared/dynamic-component/ad.directive';
import { DocumentSaveStatuses } from '@appShared/enums/documents-save-statuses.enum';
import { OibRoles } from '@appShared/enums/oib-roles.enum';
import { ModalWindowService } from '@appShared/modals-helper/modal-window.service';
import { StoreLoadStatus } from '@models/enums/store-load-status.enum';
import { Observable, Subject } from 'rxjs';
import { filter, first, takeUntil, withLatestFrom } from 'rxjs/operators';
import { ON_SUMBIT } from './const/buttons-actions.const';
import { sendingsPopupTypeComponentMap } from './const/document-sendings-popup-types.map';
import { SendingsPopupTypes } from './enums/sendings-popup-types.enum';
import { Sending } from './models/sending.model';
import { DocumentSendingsService } from './services/document-sendings.service';

@Component({
  selector: 'app-document-sendings',
  templateUrl: './document-sendings.component.html',
  styleUrls: ['./document-sendings.component.scss'],
})
export class DocumentSendingsComponent implements OnInit, OnDestroy, AfterViewInit {
  public toggleLoader: boolean;
  public permission$: Observable<PermissionModel>;

  public sendings$: Observable<Sending[]>;
  public searchInput: boolean;
  public searchText: string;
  public canSendAgain: boolean;
  public sendingsTotalCount$: Observable<number>;
  public isSearching = false;

  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(
    private redux: ReduxService,
    private toastsService: ToastsDocumentService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private documentSendingsService: DocumentSendingsService,
    private modalWindowService: ModalWindowService
  ) { }

  @ViewChild(AdDirective, { static: true }) popup: AdDirective;

  ngOnInit() {
    this.permission$ = this.documentSendingsService.getUserPermissions();
    this.getUserRoles();
    this.sendings$ = this.getSendings();
    this.subscribeToStatus();
    this.sendingsTotalCount$ = this.redux.selectStore(getSendingsTotalCount);

    this.redux.dispatchAction(new DocumentPackageSendingsGet());
  }

  ngAfterViewInit(): void {
    this.redux
      .selectStore(getSendingsPopupType)
      .pipe(withLatestFrom(this.redux.selectStore(getSendingsPopupPayload)), takeUntil(this.ngUnsubscribe))
      .subscribe(([popupType, popupPayload]) => {
        const component = sendingsPopupTypeComponentMap.get(popupType);

        if (!component) {
          this.clearPopup();
          return;
        }

        this.loadPopup(component, popupPayload);
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this.redux.dispatchAction(new DocumentPackageSendingsResetSendings());
  }

  public findSendings(): void {
    this.redux.dispatchAction(new DocumentPackageSendingsGetSearch(this.searchText));
    this.isSearching = true;
  }

  public onScrollDown(): void {
    if (this.searchText) {
      this.findSendings();
      return;
    }
    this.redux.dispatchAction(new DocumentPackageSendingsGet());
  }

  public addAddressee(): void {
    this.modalWindowService.open(AddresseeAddingPopupComponent, {}).afterClosed$
      .pipe(
        first(),
        filter(val => val.data === ON_SUMBIT),
      )
      .subscribe(() => {
        this.searchText = '';
        this.findSendings();
        this.isSearching = false;
        this.searchInput = false;
      });
  }

  public sendToAllAddressee(): void {
    this.redux.dispatchAction(new SetSendingsPopupType(SendingsPopupTypes.addresseeSendToAll));
  }

  /** Подписка на статус */
  private subscribeToStatus(): void {
    this.redux
      .selectStore(getSendingsStatus)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((status) => {
        this.toggleLoader = status === StoreLoadStatus.inProgress;

        if (status === StoreLoadStatus.error) {
          this.toastsService.addToast({ title: DocumentSaveStatuses.sendingError });
        }
      });
  }

  private getUserRoles(): void {
    this.redux
      .selectStore(getRolesForCurrentUser)
      .pipe(
        filter((r) => !!r),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((userRoles) => {
        const allowedRoles = [
          OibRoles.npaAdmin,
          OibRoles.oauEmployee,
          OibRoles.opOAUEmployee,
          OibRoles.orOAUEmployee,
          OibRoles.oirOAUEmployee,
          OibRoles.pzOAUEmployee,
        ];
        this.canSendAgain = userRoles.some((role) => allowedRoles.includes(role as OibRoles));
      });
  }

  private getSendings(): Observable<Sending[]> {
    return this.redux.selectStore(getSendings).pipe(filter((res) => !!res));
  }

  private clearPopup(): void {
    this.popup.viewContainerRef.clear();
  }

  /**
   * Динамическое создание попапов
   * @param componentClass - создаваемый компонент
   * @param payload - параметры передаваемые в поле payload создаваемых компонентов
   */
  private loadPopup(componentClass: any, payload: any): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentClass);
    this.popup.viewContainerRef.clear();
    const componentRef = this.popup.viewContainerRef.createComponent(componentFactory);

    if (payload) {
      const comp = componentRef.instance;
      comp['payload'] = payload;
    }
  }
}
