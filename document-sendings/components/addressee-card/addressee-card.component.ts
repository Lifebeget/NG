import { Component, Input, OnInit } from '@angular/core';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { getSearchText } from '@appDocuments/core/store/document-package-sendings';
import { SetSendingsPopupType } from '@appDocuments/core/store/document-package-sendings/document-package-sendings.actions';
import { SendingParamsFormType } from '@appDocuments/document-sendings/enums/sending-params-form-type.enum';
import { SendingStatus } from '@appDocuments/document-sendings/enums/sending-status.enum';
import { SendingsPopupTypes } from '@appDocuments/document-sendings/enums/sendings-popup-types.enum';
import { EdoSendingModel } from '@appDocuments/document-sendings/models/edo-sending.model';
import { EmailSendingModel } from '@appDocuments/document-sendings/models/email-sending.model';
import { PaperSendingModel } from '@appDocuments/document-sendings/models/paper-sending.model';
import { Sending } from '@appDocuments/document-sendings/models/sending.model';
import { DocumentSendingsService } from '@appDocuments/document-sendings/services/document-sendings.service';
import { PermissionModel } from '@appPermissions/models/permission.model';
import { Observable } from 'rxjs';
import { ModalWindowService } from '@appShared/modals-helper/modal-window.service';
import { AddresseeEditionPopupComponent } from '@appDocuments/document-sendings/components/popups/addressee-edition-popup/addressee-edition-popup.component';
import { first } from 'rxjs/operators';
import { DocumentPackageApiService } from '@appShared/api/document-package/document-package.api.service';

@Component({
  selector: 'app-addressee-card',
  templateUrl: './addressee-card.component.html',
  styleUrls: ['./addressee-card.component.scss'],
})
export class AddresseeCardComponent implements OnInit {
  @Input()
  sending: Sending;
  @Input()
  canSendAgain: boolean;

  public edoSending: EdoSendingModel;
  public emailSending: EmailSendingModel;
  public paperSending: PaperSendingModel;
  public noSendings: boolean;
  public isFullySent: boolean;
  public searchText$: Observable<string>;
  public permission$: Observable<PermissionModel>;
  public edoIsSended: boolean;
  public emailIsSended: boolean;
  public paperIsSended: boolean;
  public someSendingIsNotSended: boolean;

  public sendingStatuses = SendingStatus;

  private sended = [SendingStatus.SENT, SendingStatus.SENT_REPEATEDLY];

  constructor(
    private redux: ReduxService,
    private documentSendigns: DocumentSendingsService,
    private modalWindowService: ModalWindowService,
    private documentPackageApi: DocumentPackageApiService
  ) {}

  ngOnInit() {
    this.permission$ = this.documentSendigns.getUserPermissions();
    this.edoSending = this.hasEdoSending();
    this.emailSending = this.hasEmailSending();
    this.paperSending = this.hasPaperSending();
    this.isFullySent = this.documentSendigns.wasFullySent(this.emailSending, this.paperSending, this.edoSending);
    this.searchText$ = this.redux.selectStore(getSearchText);
    this.edoIsSended = this.isEdoSended();
    this.emailIsSended = this.isEmailSended();
    this.paperIsSended = this.isPaperSended();
    this.someSendingIsNotSended = !this.edoIsSended || !this.emailIsSended || !this.paperIsSended;
    this.noSendings = !this.edoSending && !this.emailSending && !this.paperSending;
  }

  public updateSending(): void {
    this.documentPackageApi
      .getDocumentPackageSending(this.sending.id)
      .pipe(first())
      .subscribe((sending) => {
        this.modalWindowService.open(AddresseeEditionPopupComponent, sending);
      });
  }

  public removeSending(): void {
    const addressee = {
      id: this.sending.id,
      employee: this.sending.employee,
      organization: this.sending.organization && this.sending.organization.shortName,
    };
    this.redux.dispatchAction(new SetSendingsPopupType(SendingsPopupTypes.addresseeRemove, addressee));
  }

  public onSendEmail(): void {
    const sending = {
      id: this.sending.id,
      type: SendingParamsFormType.email,
      employee: this.sending.employee,
      organization: this.sending.organization,
      emailSending: this.emailSending,
      edoSending: this.edoSending,
    };
    this.redux.dispatchAction(new SetSendingsPopupType(SendingsPopupTypes.addresseeSendingParams, sending));
  }

  public onSendPaper(): void {
    const sending = {
      id: this.sending.id,
      type: SendingParamsFormType.paper,
      employee: this.sending.employee,
      organization: this.sending.organization,
      paperSending: this.paperSending,
    };
    this.redux.dispatchAction(new SetSendingsPopupType(SendingsPopupTypes.addresseeSendingParams, sending));
  }

  public sendAgain(): void {
    const sending = {
      id: this.sending.id,
      employee: this.sending.employee,
      organization: this.sending.organization,
      emailSending: this.emailSending,
    };
    this.redux.dispatchAction(new SetSendingsPopupType(SendingsPopupTypes.addresseeRepeatedlySending, sending));
  }

  private isEdoSended(): boolean {
    if (!this.edoSending) {
      return true;
    }
    return this.sended.includes(this.edoSending.status);
  }

  private isEmailSended(): boolean {
    if (!this.emailSending) {
      return true;
    }
    return this.sended.includes(this.emailSending.status);
  }

  private isPaperSended(): boolean {
    if (!this.paperSending) {
      return true;
    }
    return this.sended.includes(this.paperSending.status);
  }

  private hasEdoSending(): EdoSendingModel {
    return this.sending.edoSending;
  }

  private hasEmailSending(): EmailSendingModel {
    return this.sending.emailSending;
  }

  private hasPaperSending(): PaperSendingModel {
    return this.sending.paperSending;
  }
}
