import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { FormsHelperService } from '@appCore/services/form-helper/form-helper.service';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { AddresseeTypeKeys } from '@appDocuments/document-sendings/enums/addressee-type.enum';
import { DocumentSendingsService } from '@appDocuments/document-sendings/services/document-sendings.service';
import { SendingsMapperService } from '@appDocuments/document-sendings/services/sendings-mapper.service';
import { SendingsValidators } from '@appDocuments/document-sendings/validators/sendings.validators';
import { DictionaryApiService } from '@appShared/api/dictionary/dictionary.api.service';
import { EMPLOYEE_MOCK } from 'test-mock-data/user-info/employee-base-model.mock';
import { instance, mock } from 'ts-mockito';
import { AddresseePopupBaseComponent } from './addressee-popup-base.component';
import { ModalWindowRef } from '@appShared/modals-helper/modal-window-ref';

describe('AddresseePopupBaseComponent', () => {
  let component: AddresseePopupBaseComponent;
  let fixture: ComponentFixture<AddresseePopupBaseComponent>;

  let reduxMock: ReduxService;
  let dictionaryApiMock: DictionaryApiService;
  let formsHelperServiceMock: FormsHelperService;
  let sendingMapperMock: SendingsMapperService;
  let sendingsValidatorsMock: SendingsValidators;
  let documentSendingsMock: DocumentSendingsService;
  let modalWindowRefMock: ModalWindowRef;

  beforeEach(() => {
    reduxMock = mock(ReduxService);
    dictionaryApiMock = mock(DictionaryApiService);
    formsHelperServiceMock = mock(FormsHelperService);
    sendingMapperMock = mock(SendingsMapperService);
    sendingsValidatorsMock = mock(SendingsValidators);
    documentSendingsMock = mock(DocumentSendingsService);
    modalWindowRefMock = mock(ModalWindowRef);

    TestBed.configureTestingModule({
      declarations: [AddresseePopupBaseComponent],
      imports: [ReactiveFormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        FormBuilder,
        { provide: ReduxService, useFactory: () => instance(reduxMock) },
        { provide: DictionaryApiService, useFactory: () => instance(dictionaryApiMock) },
        { provide: FormsHelperService, useFactory: () => instance(formsHelperServiceMock) },
        { provide: SendingsMapperService, useFactory: () => instance(sendingMapperMock) },
        { provide: SendingsValidators, useFactory: () => instance(sendingsValidatorsMock) },
        { provide: DocumentSendingsService, useFactory: () => instance(documentSendingsMock) },
        { provide: ModalWindowRef, useFactory: () => instance(modalWindowRefMock) },
      ],
    });

    fixture = TestBed.createComponent(AddresseePopupBaseComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('пустая форма невалидна', () => {
    expect(component.form.invalid).toBeTruthy();
  });

  it('на форме по умолчанию доступен выбор организации', () => {
    const result = component.form.value;
    expect(result.addresseeType).toBe(AddresseeTypeKeys.Organization);
    expect(result).toHaveProperty('organization');
  });

  it('валидная форма с электронной рассылкой и указанным Email', () => {
    component.form.controls.organization.setValue({
      id: 2,
      name: 'Московский городской транспорт',
      shortName: 'ГУМЧС',
    });
    component.form.controls.isEmail.setValue(true);
    component.form.controls.emailSending.setValue({ email: 'ba4riman@gmail.com', comment: '' });

    expect(component.form.valid).toBeTruthy();
  });

  it('невалидная форма с электронной рассылкой и не указанным Email', () => {
    component.form.controls.isEmail.setValue(true);

    expect(component.form.valid).toBeFalsy();
  });

  it('доступность полей при выборе ЭДО', () => {
    component.form.controls.isEdo.setValue(true);

    const result = component.form.value.edoSending;

    expect(result).toBeDefined();
    expect(result.comment).toBe('');
  });

  it('доступность полей при выборе бумажной рассылки', () => {
    component.form.controls.isPaper.setValue(true);

    const result = component.form.value.paperSending;

    expect(result).toBeDefined();
    expect(result.address).toBeNull();
    expect(result.comment).toBe('');
    expect(result.quantity).toBe(1);
  });

  it('доступность полей при выборе электронной рассылки', () => {
    component.form.controls.isEmail.setValue(true);

    const result = component.form.value.emailSending;

    expect(result).toBeDefined();
    expect(result.email).toBeNull();
    expect(result.comment).toBe('');
  });

  it('при выборе электронной рассылки для пользователя автоматически заполняется поле email при наличии', () => {
    component.form.controls.addresseeType.setValue(AddresseeTypeKeys.Personally);
    component.form.controls.employee.setValue(EMPLOYEE_MOCK);
    component.form.controls.isEmail.setValue(true);

    const result = component.form.value.emailSending;

    expect(result.email).toBe('test@mail');
  });
});
