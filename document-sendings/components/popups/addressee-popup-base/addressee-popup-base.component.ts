import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Unsubscriber } from '@appCore/glb/unsubscriber';
import { FormsHelperService } from '@appCore/services/form-helper/form-helper.service';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { AddresseeType, AddresseeTypeKeys } from '@appDocuments/document-sendings/enums/addressee-type.enum';
import { DocumentSendingsService } from '@appDocuments/document-sendings/services/document-sendings.service';
import { SendingsMapperService } from '@appDocuments/document-sendings/services/sendings-mapper.service';
import { emptyFormValidator } from '@appDocuments/document-sendings/validators/empty-form.validator';
import { SendingsValidators } from '@appDocuments/document-sendings/validators/sendings.validators';
import { DictionaryApiService } from '@appShared/api/dictionary/dictionary.api.service';
import { ModalWindowRef } from '@appShared/modals-helper/modal-window-ref';
import { DictionaryModel } from '@models/base/dictionary.model';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

/**
 * Базовый компонент для добавления и редактирования адресатов
 * Вызов попапов реализуется через наследников этого компонента
 */
@Component({
  selector: 'app-addressee-popup-base.component.ts',
  templateUrl: './addressee-popup-base.component.html',
  styleUrls: ['./addressee-popup-base.component.scss'],
})
export class AddresseePopupBaseComponent extends Unsubscriber implements OnInit {
  public form: FormGroup;
  public addresseeTypeKeys = AddresseeTypeKeys;
  public addresseeTypeOptions: Map<string, string> = new Map<string, string>();
  public organizations$: Observable<DictionaryModel[]>;
  public hasEdoSendingComment = false;
  public hasEmailSendingComment = false;
  public hasPaperSendingComment = false;
  public title: string;

  constructor(
    protected redux: ReduxService,
    protected formBuilder: FormBuilder,
    protected dictionaryApi: DictionaryApiService,
    protected formsHelperService: FormsHelperService,
    protected sendingMapper: SendingsMapperService,
    protected sendingsValidators: SendingsValidators,
    protected documentSendings: DocumentSendingsService,
    private modalWindowRef: ModalWindowRef
  ) {
    super();
  }

  ngOnInit() {
    this.initAddresseeTypeOptions();
    this.organizations$ = this.documentSendings.getOrganizationsList(true);
    this.form = this.initForm();
    this.formChangesSubscribe();
    this.formsHelperService.markAsTouchedDeep(this.form);
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  /* Инициализирует опции для addresseeType */
  private initAddresseeTypeOptions(): void {
    for (const key in AddresseeType) {
      if (AddresseeType[key]) {
        this.addresseeTypeOptions.set(key, AddresseeType[key]);
      }
    }
  }
  public get employeeControl() {
    return this.form?.get('employee') as FormArray;
  }
  public get hasOrganizationError(): string {
    return this.form.get('organization') && this.form.get('organization').getError('hasOrganization');
  }

  public get hasEmployeeError(): string {
    return this.form.get('employee') && this.form.get('employee').getError('hasEmployee');
  }

  public closePopup(action?: string): void {
    this.modalWindowRef.close(action);
  }

  /**
   * Выбор/снятие выбора для рассылки ЭДО
   */
  public toggleEdoSendingComment(): void {
    this.hasEdoSendingComment = !this.hasEdoSendingComment;
  }

  /**
   * Выбор/снятие выбора для рассылки по email
   */
  public toggleEmailSendingComment(): void {
    this.hasEmailSendingComment = !this.hasEmailSendingComment;
  }

  /**
   * Выбор/снятие выбора для бумажной рассылки
   */
  public togglePaperSendingComment(): void {
    this.hasPaperSendingComment = !this.hasPaperSendingComment;
  }

  /**
   * Метод переопределяется в классах наслдениках.
   * Нужен для пердотвращения ошибки в шаблоне.
   */
  public onSubmit(): void {}

  private initForm(): FormGroup {
    return this.formBuilder.group(
      {
        addresseeType: this.addresseeTypeKeys.Organization,
        organization: [null, [Validators.required]],
        isApprovalSheet: false,
        isEdo: true,
        isPaper: false,
        isEmail: false,
      },
      {
        validators: emptyFormValidator,
      }
    );
  }

  /**
   * Добавлнение полей ЭДО
   */
  private appendEdoSendingSettings(): FormGroup {
    return this.formBuilder.group({
      comment: '',
    });
  }

  /**
   * Добавлнение полей бумажной рассылки
   */
  private appendPaperSendingSettings(): FormGroup {
    return this.formBuilder.group({
      address: [null, [Validators.required]],
      quantity: [1, [Validators.required, Validators.min(1), Validators.pattern('^[0-9]*$'), Validators.maxLength(18)]],
      comment: '',
    });
  }

  /**
   * Добавлнение полей электронной рассылки
   */
  private appendEmailSendingSettings(): FormGroup {
    return this.formBuilder.group({
      email: [this.getSelectedEmployeeEmail(), [Validators.required, Validators.email]],
      comment: '',
    });
  }

  private formChangesSubscribe(): void {
    this.addresseeTypeChanges();
    this.sendingSettingsChanges();
  }

  /**
   * Переключение форм для рассылки лично/организации
   */
  private addresseeTypeChanges(): void {
    this.form
      .get('addresseeType')
      .valueChanges.pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((type: AddresseeTypeKeys) => {
        if (type === AddresseeTypeKeys.Personally) {
          this.form.removeControl('organization');
          this.form.addControl('employee', this.formBuilder.control(null, [Validators.required]));
          this.emailSendingAutoFill();
        }

        if (type === AddresseeTypeKeys.Organization) {
          this.form.removeControl('employee');
          this.form.addControl('organization', this.formBuilder.control(null, [Validators.required]));
        }
        this.formsHelperService.markAsTouchedDeep(this.form);
      });
  }

  /**
   * Автозаполенние поля email электронной рассылки, если выбран сотрудник
   */
  private emailSendingAutoFill(): void {
    this.employeeControl.valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
      const emailSendingControl = this.form.get('emailSending');
      const hasEmptyEmailControl = emailSendingControl && !emailSendingControl.get('email').value;
      if (hasEmptyEmailControl) {
        emailSendingControl.patchValue({ email: this.getSelectedEmployeeEmail() }, { emitEvent: false });
      }
    });
  }

  /**
   * Получение email выбранного на форме пользователя
   */
  private getSelectedEmployeeEmail(): string {
    return this.form.get('employee') && this.form.get('employee').value && this.form.get('employee').value.email;
  }

  /**
   * Включение/выключение форм для бумажной рассылки и рассылки по email
   */
  private sendingSettingsChanges(): void {
    const isEdoControl = this.form.get('isEdo');
    const isPaperControl = this.form.get('isPaper');
    const isEmailControl = this.form.get('isEmail');

    if (isEdoControl.value) {
      this.form.addControl('edoSending', this.appendEdoSendingSettings());
    }

    isEdoControl.valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe((status) => {
      if (status) {
        this.form.addControl('edoSending', this.appendEdoSendingSettings());
      } else {
        this.form.removeControl('edoSending');
      }
    });

    isPaperControl.valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe((status) => {
      if (status) {
        this.form.addControl('paperSending', this.appendPaperSendingSettings());
        this.form.get('paperSending').markAllAsTouched();
      } else {
        this.form.removeControl('paperSending');
      }
    });

    isEmailControl.valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe((status) => {
      if (status) {
        this.form.addControl('emailSending', this.appendEmailSendingSettings());
        this.form.get('emailSending').markAllAsTouched();
      } else {
        this.form.removeControl('emailSending');
      }
    });
  }
}
