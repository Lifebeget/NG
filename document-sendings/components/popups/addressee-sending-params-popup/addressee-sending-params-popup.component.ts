import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ReduxService } from '@appCore/services/redux/redux.service';
import {
  DocumentPackageSendingSendEmail,
  DocumentPackageSendingSendPaper,
  SetSendingsPopupType,
} from '@appDocuments/core/store/document-package-sendings/document-package-sendings.actions';
import { SendingParamsFormType } from '@appDocuments/document-sendings/enums/sending-params-form-type.enum';
import { SendingParamsModel } from '@appDocuments/document-sendings/models/sending-params.model';
import { DocumentSendingsService } from '@appDocuments/document-sendings/services/document-sendings.service';
import { SendingsFormService } from '@appDocuments/document-sendings/services/sendings-form.service';
import { SendingsMapperService } from '@appDocuments/document-sendings/services/sendings-mapper.service';

@Component({
  selector: 'app-addressee-sending-params-popup',
  templateUrl: './addressee-sending-params-popup.component.html',
  styleUrls: ['./addressee-sending-params-popup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddresseeSendingParamsPopupComponent implements OnInit {
  public payload: SendingParamsModel;
  public form: FormGroup;
  public sendingParamsFormType = SendingParamsFormType;

  public title: string;

  constructor(
    private redux: ReduxService,
    private sendingsMapper: SendingsMapperService,
    private sendingsForm: SendingsFormService,
    private documentSendigns: DocumentSendingsService
  ) {}

  ngOnInit() {
    const isPaper = this.payload.type === SendingParamsFormType.paper;
    this.form = this.sendingsForm.initSendingParamsForm(isPaper, this.payload.id);
    this.title = this.documentSendigns.getTitle(this.payload);
  }

  public closePopup(): void {
    this.redux.dispatchAction(new SetSendingsPopupType(null));
  }

  public onApply(): void {
    if (this.payload.type === SendingParamsFormType.email) {
      const emailSendingParams = this.sendingsMapper.mapEmailSendingParamsForm(this.form, this.payload.id);
      this.redux.dispatchAction(new DocumentPackageSendingSendEmail(emailSendingParams));
    }
    if (this.payload.type === SendingParamsFormType.paper) {
      const paperSendingParams = this.sendingsMapper.mapPaperSendingParamsForm(this.form, this.payload.paperSending.id);
      this.redux.dispatchAction(new DocumentPackageSendingSendPaper(paperSendingParams));
    }
    this.closePopup();
  }
}
