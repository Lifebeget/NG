import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Unsubscriber } from '@appCore/glb/unsubscriber';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { getDocumentPackageId } from '@appDocuments/core/store/document-package';
import { getUnsentSendings } from '@appDocuments/core/store/document-package-sendings';
import {
  DocumentPackageSendingMassSend,
  GetUnsentSendingsCount,
  SetSendingsPopupType,
} from '@appDocuments/core/store/document-package-sendings/document-package-sendings.actions';
import { UnsentSendingsModel } from '@appDocuments/document-sendings/models/unsent-sendings.model';
import { SendingsFormService } from '@appDocuments/document-sendings/services/sendings-form.service';
import { SendingsMapperService } from '@appDocuments/document-sendings/services/sendings-mapper.service';
import { Observable } from 'rxjs';
import { filter, first, map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-send-to-all-popup',
  templateUrl: './addressee-send-to-all-popup.component.html',
  styleUrls: ['./addressee-send-to-all-popup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddresseeSendToAllPopupComponent extends Unsubscriber implements OnInit, OnDestroy {
  public form: FormGroup;
  public isUnsentEmail$: Observable<boolean>;
  public isUnsentPaper$: Observable<boolean>;
  public unsentSendings$: Observable<UnsentSendingsModel>;
  public edoSpecifyAsSentLabel$: Observable<string>;
  public emailSpecifyAsSentLabel$: Observable<string>;
  public paperSpecifyAsSentLabel$: Observable<string>;
  public sendingDisabled = true;

  constructor(
    private redux: ReduxService,
    private sendingsForm: SendingsFormService,
    private sendingsMapper: SendingsMapperService
  ) {
    super();
  }

  ngOnInit(): void {
    this.form = this.initForm();
    this.unsentSendings$ = this.getUnsentSendings();
    this.hasUnsentSendings();
    this.redux
      .selectStore(getDocumentPackageId)
      .pipe(first())
      .subscribe((documentPackageId) => this.redux.dispatchAction(new GetUnsentSendingsCount(documentPackageId)));
    this.edoSpecifyAsSentLabel$ = this.edoSpecifyAsSent();
    this.emailSpecifyAsSentLabel$ = this.emailSpecifyAsSent();
    this.paperSpecifyAsSentLabel$ = this.paperSpecifyAsSent();
    this.getSendingAvailability();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  public closePopup(): void {
    this.redux.dispatchAction(new SetSendingsPopupType(null));
  }

  public onApply(): void {
    this.redux
      .selectStore(getDocumentPackageId)
      .pipe(
        first(),
        map((documentPackageId) => this.sendingsMapper.mapMassSendignParamsForm(this.form, documentPackageId))
      )
      .subscribe((massSending) => {
        this.redux.dispatchAction(new DocumentPackageSendingMassSend(massSending));
      });
    this.closePopup();
  }

  private initForm(): FormGroup {
    return this.sendingsForm.initMassSendForm();
  }

  private hasUnsentSendings(): void {
    this.unsentSendings$
      .pipe(
        filter((unsentSendings) => !!unsentSendings),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((unsentSendings) => {
        !unsentSendings.countEdo ? this.form.get('isEdo').disable() : this.form.get('isEdo').enable();
        !unsentSendings.countEmail ? this.form.get('isEmail').disable() : this.form.get('isEmail').enable();
        !unsentSendings.countPaper ? this.form.get('isPaper').disable() : this.form.get('isPaper').enable();
      });
  }

  private getUnsentSendings(): Observable<UnsentSendingsModel> {
    return this.redux.selectStore(getUnsentSendings).pipe(filter((unsentSendings) => !!unsentSendings));
  }

  private edoSpecifyAsSent(): Observable<string> {
    return this.unsentSendings$.pipe(
      map(
        (unsentSendings) =>
          `Указать рассылку по ЭДО как отправленную ${
            unsentSendings.countEdo ? `(адресов: ${unsentSendings.countEdo})` : '(нет неотправленных)'
          }`
      )
    );
  }

  private emailSpecifyAsSent(): Observable<string> {
    return this.unsentSendings$.pipe(
      map(
        (unsentSendings) =>
          `Указать электронную рассылку как отправленную ${
            unsentSendings.countEmail ? `(адресов: ${unsentSendings.countEmail})` : '(нет неотправленных)'
          }`
      )
    );
  }

  private paperSpecifyAsSent(): Observable<string> {
    return this.unsentSendings$.pipe(
      map(
        (unsentSendings) =>
          `Указать бумажную рассылку как отправленную ${
            unsentSendings.countPaper ? `(адресов: ${unsentSendings.countPaper})` : '(нет неотправленных)'
          }`
      )
    );
  }

  private getSendingAvailability(): void {
    this.form.valueChanges.pipe(takeUntil(this.ngUnsubscribe)).subscribe((form) => {
      this.sendingDisabled = !(form.isEmail || form.isPaper || form.isEdo) || !form.sending.date;
    });
  }
}
