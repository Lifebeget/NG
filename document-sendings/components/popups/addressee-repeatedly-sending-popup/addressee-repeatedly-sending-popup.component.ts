import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Unsubscriber } from '@appCore/glb/unsubscriber';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { getSendingById } from '@appDocuments/core/store/document-package-sendings';
import {
  RepeatedlySend,
  SetSendingsPopupType,
} from '@appDocuments/core/store/document-package-sendings/document-package-sendings.actions';
import { RepeatedSendingModel } from '@appDocuments/document-sendings/models/repeated-sending.model';
import { SendingParamsModel } from '@appDocuments/document-sendings/models/sending-params.model';
import { Sending } from '@appDocuments/document-sendings/models/sending.model';
import { DocumentSendingsService } from '@appDocuments/document-sendings/services/document-sendings.service';
import { SendingsFormService } from '@appDocuments/document-sendings/services/sendings-form.service';
import { SendingsMapperService } from '@appDocuments/document-sendings/services/sendings-mapper.service';
import { Observable } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-addressee-repeatedly-sending-popup',
  templateUrl: './addressee-repeatedly-sending-popup.component.html',
  styleUrls: ['./addressee-repeatedly-sending-popup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddresseeRepeatedlySendingPopupComponent extends Unsubscriber implements OnInit, OnDestroy {
  public payload: SendingParamsModel;
  public form: FormGroup;
  public title: string;

  private sending$: Observable<Sending>;

  constructor(
    private sendingsFormService: SendingsFormService,
    private documentSendigns: DocumentSendingsService,
    private redux: ReduxService,
    private sendingsMapperService: SendingsMapperService
  ) {
    super();
  }

  ngOnInit() {
    this.sending$ = this.redux.selectStore(getSendingById(this.payload.id));
    this.form = this.sendingsFormService.initMassSendForm();
    this.disabledFormFields();
    this.title = this.documentSendigns.getTitleRepeatedlySending(this.payload);
  }

  ngOnDestroy() {
    this.unsubscribe();
  }

  public closePopup(): void {
    this.redux.dispatchAction(new SetSendingsPopupType(null));
  }

  public onApply(): void {
    const formValue = this.form.value;
    const repeatedSending: RepeatedSendingModel = {
      sendingId: this.payload.id,
      isEdo: formValue.isEdo,
      isPaper: formValue.isPaper,
      isEmail: formValue.isEmail,
      sendingDate: this.sendingsMapperService.mapDatetimeSending(formValue.sending.date, formValue.sending.time),
    };

    this.redux.dispatchAction(new RepeatedlySend(repeatedSending));
    this.closePopup();
  }

  private disabledFormFields(): void {
    this.sending$
      .pipe(
        filter((sending) => !!sending),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((sending) => {
        if (!sending.edoSending) {
          this.form.get('isEdo').disable();
        }
        if (!sending.emailSending) {
          this.form.get('isEmail').disable();
        }
        if (!sending.paperSending) {
          this.form.get('isPaper').disable();
        }
      });
  }
}
