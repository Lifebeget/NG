import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormsHelperService } from '@appCore/services/form-helper/form-helper.service';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { DocumentPackageSendingsUpdate } from '@appDocuments/core/store/document-package-sendings/document-package-sendings.actions';
import { AddresseeTypeKeys } from '@appDocuments/document-sendings/enums/addressee-type.enum';
import { Sending } from '@appDocuments/document-sendings/models/sending.model';
import { DocumentSendingsService } from '@appDocuments/document-sendings/services/document-sendings.service';
import { SendingsFormService } from '@appDocuments/document-sendings/services/sendings-form.service';
import { SendingsMapperService } from '@appDocuments/document-sendings/services/sendings-mapper.service';
import { SendingsValidators } from '@appDocuments/document-sendings/validators/sendings.validators';
import { DictionaryApiService } from '@appShared/api/dictionary/dictionary.api.service';
import { ModalWindowRef } from '@appShared/modals-helper/modal-window-ref';
import { takeUntil } from 'rxjs/operators';
import { AddresseePopupBaseComponent } from '../addressee-popup-base/addressee-popup-base.component';

/**
 * Реализация попапа для редактирования адресата рассылки
 */
@Component({
  selector: 'app-addressee-adding-popup',
  templateUrl: '../addressee-popup-base/addressee-popup-base.component.html',
  styleUrls: ['../addressee-popup-base/addressee-popup-base.component.scss'],
})
export class AddresseeEditionPopupComponent extends AddresseePopupBaseComponent implements OnInit {
  private sending: Sending;

  constructor(
    redux: ReduxService,
    formBuilder: FormBuilder,
    dictionaryApi: DictionaryApiService,
    formsHelperService: FormsHelperService,
    sendingMapper: SendingsMapperService,
    sendingsValidators: SendingsValidators,
    documentSendings: DocumentSendingsService,
    modalWindowRef: ModalWindowRef,
    private sendingForm: SendingsFormService,
    private cdr: ChangeDetectorRef
  ) {
    super(
      redux,
      formBuilder,
      dictionaryApi,
      formsHelperService,
      sendingMapper,
      sendingsValidators,
      documentSendings,
      modalWindowRef
    );
    this.sending = modalWindowRef.data;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.title = 'Редактирование адресата';
    this.getAddresseeInfo();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public onSubmit(): void {
    if (!this.sending) {
      return;
    }
    const mappedForm = this.sendingMapper.mapEditionSendingForm(this.form, this.sending);
    this.redux.dispatchAction(new DocumentPackageSendingsUpdate(mappedForm));
    this.closePopup();
  }

  private getAddresseeInfo(): void {
    this.sendingForm.fillEditionForm(this.sending, this.form);

    this.setAsyncValidators(this.sending);

    if (this.sending.edoSending) {
      this.hasEdoSendingComment = !!this.sending.edoSending.comment;
    }

    if (this.sending.emailSending) {
      this.hasEmailSendingComment = !!this.sending.emailSending.comment;
    }

    if (this.sending.paperSending) {
      this.hasPaperSendingComment = !!this.sending.paperSending.comment;
    }
  }

  /**
   * Переключение форм для рассылки лично/организации
   */
  private setAsyncValidators(sending: Sending): void {
    if (sending.employee && this.form.get('employee')) {
      this.form.get('employee').setAsyncValidators(this.sendingsValidators.hasEmployeeAsyncValidator(sending.employee));
    }

    if (sending.organization && this.form.get('organization')) {
      this.form
        .get('organization')
        .setAsyncValidators(this.sendingsValidators.hasOrganizationAsyncValidator(sending.organization));
    }

    this.form
      .get('addresseeType')
      .valueChanges.pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((type: AddresseeTypeKeys) => {
        if (type === AddresseeTypeKeys.Personally) {
          this.form
            .get('employee')
            .setAsyncValidators(this.sendingsValidators.hasEmployeeAsyncValidator(sending.employee));
        }

        if (type === AddresseeTypeKeys.Organization) {
          this.form
            .get('organization')
            .setAsyncValidators(this.sendingsValidators.hasOrganizationAsyncValidator(sending.organization));
        }
      });

    this.sendingsValidators.changeDetectionSubject.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
      this.cdr.markForCheck();
    });
  }
}
