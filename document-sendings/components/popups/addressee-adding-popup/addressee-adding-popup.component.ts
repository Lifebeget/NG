import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormsHelperService } from '@appCore/services/form-helper/form-helper.service';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { DocumentPackageSendingAdd } from '@appDocuments/core/store/document-package-sendings/document-package-sendings.actions';
import { AddresseeTypeKeys } from '@appDocuments/document-sendings/enums/addressee-type.enum';
import { DocumentSendingsService } from '@appDocuments/document-sendings/services/document-sendings.service';
import { SendingsMapperService } from '@appDocuments/document-sendings/services/sendings-mapper.service';
import { SendingsValidators } from '@appDocuments/document-sendings/validators/sendings.validators';
import { DictionaryApiService } from '@appShared/api/dictionary/dictionary.api.service';
import { takeUntil } from 'rxjs/operators';
import { AddresseePopupBaseComponent } from '../addressee-popup-base/addressee-popup-base.component';
import { ModalWindowRef } from '@appShared/modals-helper/modal-window-ref';
import { ON_SUMBIT } from '@appDocuments/document-sendings/const/buttons-actions.const';

/**
 * Реализация попапа для добавления адресата рассылки
 */
@Component({
  selector: 'app-addressee-adding-popup',
  templateUrl: '../addressee-popup-base/addressee-popup-base.component.html',
  styleUrls: ['../addressee-popup-base/addressee-popup-base.component.scss'],
})
export class AddresseeAddingPopupComponent extends AddresseePopupBaseComponent implements OnInit {
  constructor(
    private cdr: ChangeDetectorRef,
    redux: ReduxService,
    formBuilder: FormBuilder,
    dictionaryApi: DictionaryApiService,
    formsHelperService: FormsHelperService,
    sendingMapper: SendingsMapperService,
    sendingsValidators: SendingsValidators,
    documentSendings: DocumentSendingsService,
    modalWindowRef: ModalWindowRef,
  ) {
    super(redux, formBuilder, dictionaryApi, formsHelperService, sendingMapper, sendingsValidators, documentSendings, modalWindowRef);
  }

  ngOnInit() {
    super.ngOnInit();
    this.title = 'Добавление адресата';
    this.setAsyncValidators();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public onSubmit(): void {
    const mappedForm = this.sendingMapper.mapAddingSendingForm(this.form);
    this.redux.dispatchAction(new DocumentPackageSendingAdd(mappedForm));
    this.closePopup(ON_SUMBIT);
  }

  /**
   * Переключение форм для рассылки лично/организации
   */
  private setAsyncValidators(): void {
    if (this.form.get('employee')) {
      this.form.get('employee').setAsyncValidators(this.sendingsValidators.hasEmployeeAsyncValidator());
    }

    if (this.form.get('organization')) {
      this.form.get('organization').setAsyncValidators(this.sendingsValidators.hasOrganizationAsyncValidator());
    }

    this.form
      .get('addresseeType')
      .valueChanges.pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((type: AddresseeTypeKeys) => {
        if (type === AddresseeTypeKeys.Personally) {
          this.form.get('employee').setAsyncValidators(this.sendingsValidators.hasEmployeeAsyncValidator());
        }

        if (type === AddresseeTypeKeys.Organization) {
          this.form.get('organization').setAsyncValidators(this.sendingsValidators.hasOrganizationAsyncValidator());
        }
      });

    this.sendingsValidators.changeDetectionSubject.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
      this.cdr.markForCheck();
    });
  }

}
