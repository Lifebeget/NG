import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ReduxService } from '@appCore/services/redux/redux.service';
import {
  DocumentPackageSendingRemove,
  SetSendingsPopupType,
} from '@appDocuments/core/store/document-package-sendings/document-package-sendings.actions';

@Component({
  selector: 'app-addressee-remove-popup',
  templateUrl: './addressee-remove-popup.component.html',
  styleUrls: ['./addressee-remove-popup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddresseeRemovePopupComponent {
  public payload: any;

  constructor(private redux: ReduxService) {}

  public closePopup(): void {
    this.redux.dispatchAction(new SetSendingsPopupType(null));
  }

  public onRemove(): void {
    this.redux.dispatchAction(new DocumentPackageSendingRemove(this.payload.id));
    this.closePopup();
  }
}
