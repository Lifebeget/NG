import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-sending-params-form',
  templateUrl: './sending-params-form.component.html',
  styleUrls: ['./sending-params-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SendingParamsFormComponent {
  @Input()
  date: string;
  @Input()
  time: string;
  @Input()
  quantity: number;
}
