import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EditorDocumentPackageComponent } from '@appDocuments/document-editor/editor-document-package.component';

@NgModule({
  imports: [CommonModule],
  declarations: [EditorDocumentPackageComponent]
})
export class DocumentEditorModule {}
