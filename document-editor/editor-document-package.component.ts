import { Component, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { ThirdPartyService } from '@appCore/services/third-party/third-party.service';
import { getDocumentPackageId } from '@appDocuments/core/store/document-package';
import { getCompositionDocumentByLdeDocumentId } from '@appDocuments/documents-composition/store/composition-documents';
import { CompositionDocumentsCopiesActionReset } from '@appDocuments/documents-composition/store/composition-documents-copies/composition-documents-copies.actions';
import {
  CompositionDocumentsActionLoadVersion,
  CompositionDocumentsActionReset
} from '@appDocuments/documents-composition/store/composition-documents/composition-documents.actions';
import { EditorBaseComponent } from '@appShared/components/editor-base/editor-base.component';
import { Observable } from 'rxjs';
import { filter, first, switchMap } from 'rxjs/operators';
import { AgreementRouteUsersStoreService } from '../../core/entity-store/facade-services/agreement-route-users-store.service';
import { DocumentPackageEditingParticipantStoreService } from '../../core/entity-store/facade-services/document-package-editing-participant-store.service';

/** Компонент редактора для отображения в пакете документов */
@Component({
  selector: 'app-editor',
  templateUrl: '../../shared/components/editor-base/editor-base.component.html',
  styleUrls: ['../../shared/components/editor-base/editor-base.component.scss']
})
export class EditorDocumentPackageComponent extends EditorBaseComponent implements OnDestroy {
  constructor(
    router: Router,
    route: ActivatedRoute,
    redux: ReduxService,
    ngZone: NgZone,
    thirdPartyService: ThirdPartyService,
    editorsService: DocumentPackageEditingParticipantStoreService,
    agreementRouteUsersService: AgreementRouteUsersStoreService,
  ) {
    super(router, route, redux, ngZone, thirdPartyService, editorsService, agreementRouteUsersService);
  }

  /** обновить ревизию у документов в составе сущности ПДокументов */
  protected updateRevisionInStoreHook(): void {
    this.redux
      .selectStore(getCompositionDocumentByLdeDocumentId(this.ldeDocumentId))
      .pipe(
        first(),
        filter(document => !!document)
      )
      .subscribe(document => this.redux.dispatchAction(new CompositionDocumentsActionLoadVersion(document.id)));
  }

  /** узнать фазу маршрута ПД */
  protected isAccessAllowedHook(): Observable<boolean> {
    const ldeDocumentId = this.route.snapshot.params['ldeDocumentId'];
    const revisionId = this.route.snapshot.queryParams['revisionId'];
    return this.redux.selectStore(getDocumentPackageId).pipe(
      first(),
      filter(i => !!i),
      switchMap(documentPackageId => {
        return this.editorsService.hasPackDocumentEditor$(documentPackageId, ldeDocumentId, revisionId);
      })
    );
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.redux.dispatchAction(new CompositionDocumentsActionReset());
    this.redux.dispatchAction(new CompositionDocumentsCopiesActionReset());
  }
}
