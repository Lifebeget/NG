import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { documentEditPageReducer } from '@appDocuments/document-edit/store/document-edit-page.reducer';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '../../shared/shared.module';
import { DocumentsSharedModule } from '../shared/documents-shared.module';
import { ReviewDateChangeRequestPopupComponent } from './components/review-date-change-request-popup/review-date-change-request-popup.component';
import { DocumentEditComponent } from './document-edit.component';
import { ResponseToFormValueMapperService } from './services/response-to-form-value-mapper.service';
import { DocumentEditPipe } from './pipe/document-edit.pipe';

@NgModule({
  imports: [
    StoreModule.forFeature('documentEditPage', documentEditPageReducer),
    CommonModule,
    DocumentsSharedModule,
    SharedModule,
    ReactiveFormsModule,
  ],
  declarations: [DocumentEditComponent, ReviewDateChangeRequestPopupComponent, DocumentEditPipe],
  providers: [ResponseToFormValueMapperService],
})
export class DocumentEditModule {}
