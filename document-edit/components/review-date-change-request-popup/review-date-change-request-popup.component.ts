import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { PopupClose, PopupOpen } from '@appCore/store/popup/popup.actions';
import { ReviewDateChangeRequestModel } from '@appDocuments/shared/models/review-date-change-request.model';
import { ReviewDateFormModel } from '@appDocuments/shared/models/review-date-form.model';
import { CalendarDateColors } from '@appShared/components/controls/input-calendar/enums/calendar-date-colors.enum';
import { HighlightedDateModel } from '@appShared/components/controls/input-calendar/models/highlighted-date.model';
import { getMeetingDates } from '@appShared/components/controls/input-calendar/store/meeting-dates';
import moment from 'moment';
import { filter, first, map } from 'rxjs/operators';

@Component({
  selector: 'app-review-date-change-request-popup',
  templateUrl: './review-date-change-request-popup.component.html',
  styleUrls: ['./review-date-change-request-popup.component.scss']
})
export class ReviewDateChangeRequestPopupComponent implements OnInit, OnDestroy {
  @Input()
  minDate = moment(new Date());
  @Input()
  documentPackageId: number;
  @Input()
  oldReviewDate: ReviewDateFormModel;
  @Input()
  newReviewDate: ReviewDateFormModel;
  @Output()
  cancelCallback: EventEmitter<null> = new EventEmitter();
  @Output()
  successCallback: EventEmitter<ReviewDateChangeRequestModel> = new EventEmitter();

  public requestForm: FormGroup;
  public meetingPmDates: HighlightedDateModel[];

  constructor(private redux: ReduxService, private fb: FormBuilder) {}

  ngOnInit() {
    this.redux.dispatchAction(new PopupOpen());
    this.getMeetingDatesFromStore();
    this.requestForm = this.initForm();
  }

  ngOnDestroy() {
    this.redux.dispatchAction(new PopupClose());
  }

  public closePopup(): void {
    this.cancelCallback.emit();
  }

  public onConfirm(): void {
    this.successCallback.emit(this.requestForm.value);
    this.closePopup();
  }

  private initForm(): FormGroup {
    const { plannedReviewDate, approvalForm } = this.newReviewDate;

    return this.fb.group({
      documentPackage: { id: this.documentPackageId },
      currentReviewDate: [
        {
          value: this.oldReviewDate.plannedReviewDate,
          disabled: true
        }
      ],
      updatedReviewDate: [plannedReviewDate, this.highlightedDatesValidator(this.meetingPmDates, approvalForm.id)],
      updatedApprovalForm: { id: approvalForm.id },
      changeReason: [null, Validators.required]
    });
  }

  private highlightedDatesValidator(highlightedDates: HighlightedDateModel[], approvalFormId: number): ValidatorFn {
    return (formControl: FormControl): ValidationErrors => {
      const value = formControl.value;
      if (!value) {
        return { invalidDate: 'Поле не должно быть пустым' };
      }

      const meetingType = approvalFormId === 1 ? 'Правительства Москвы' : 'Президиума Правительства Москвы';
      const errorObj = { invalidDate: `На выбранную дату нет заседания ${meetingType}` };

      const dateValueIntance = moment(value).format('YYYY-MM-DD');
      const isDateValid = highlightedDates.some(dateObj => dateObj.date === dateValueIntance);

      return isDateValid ? null : errorObj;
    };
  }

  private getMeetingDatesFromStore(): void {
    this.redux
      .selectStore(getMeetingDates)
      .pipe(
        first(),
        filter(info => !!info),
        map(datesArr => datesArr.map(dateStr => ({ date: dateStr, color: CalendarDateColors.green })))
      )
      .subscribe(dates => (this.meetingPmDates = dates));
  }
}
