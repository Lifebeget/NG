import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from './document-edit-page.reducer';

const documentEditPageSelector = createFeatureSelector('documentEditPage');

export const getToggleLoader = createSelector(documentEditPageSelector, (state: State) => state.toggleLoader);
export const getToggleSuccessPopup = createSelector(
  documentEditPageSelector,
  (state: State) => state.toggleSuccessSavePopup
);
