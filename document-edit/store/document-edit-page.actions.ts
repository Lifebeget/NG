import { Action } from '@ngrx/store';

export enum DocumentEditPageActionTypes {
  ToggleLoader = '[Document Edit Page] ToggleLoader',
  ToggleSuccessSavePopup = '[Document Edit Page] ToggleSuccessSavePopup',
}

export class ToggleLoader implements Action {
  readonly type = DocumentEditPageActionTypes.ToggleLoader;

  constructor(public payload: boolean) {}
}

export class ToggleSuccessSavePopup implements Action {
  readonly type = DocumentEditPageActionTypes.ToggleSuccessSavePopup;

  constructor(public payload: boolean) {}
}

export type DocumentEditPageActions = ToggleLoader | ToggleSuccessSavePopup;
