import { DocumentEditPageActionTypes, DocumentEditPageActions } from './document-edit-page.actions';

export interface State {
  toggleLoader: boolean;
  toggleSuccessSavePopup: boolean;
}

const initialState: State = {
  toggleLoader: false,
  toggleSuccessSavePopup: false,
};

export function documentEditPageReducer(state = initialState, action: DocumentEditPageActions): State {
  switch (action.type) {
    case DocumentEditPageActionTypes.ToggleLoader: {
      return {
        ...state,
        toggleLoader: action.payload,
      };
    }

    case DocumentEditPageActionTypes.ToggleSuccessSavePopup: {
      return {
        ...state,
        toggleSuccessSavePopup: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}
