import { Inject, Pipe, PipeTransform } from '@angular/core';
import { RoutePhaseTypes } from '@appDocuments/documents-route/enums/route-phase-types.enum';
import { VertexState } from '@appDocuments/documents-route/enums/vertex-state.enum';
import { VertexResponseModel } from '@appShared/api/documents-route/models/route-tree.response/vertex.response.model';
import { OibRoles } from '@models/enums/oib-roles.enum';
import { CURRENT_USER_ROLES } from '@appRouting/guards/auth-guard.service';

@Pipe({
  name: 'documentEdit',
})
export class DocumentEditPipe implements PipeTransform {
  constructor(@Inject(CURRENT_USER_ROLES) private current_user_roles: Map<string, string>) {}

  transform(
    getLastPointForCurrentUser: VertexResponseModel,
    isEdit: boolean,
    isConsiderationInfoLoading: boolean
  ): boolean {
    /* Пользователь с ролью ОИБ "Администратор" может редактировать сведения любых ПД
    /* https://confluence.it2g.ru/x/hhyDBQ (п.1, пп 2.3)
     */
    const hasUserRoleAdministrator = this.current_user_roles.has(OibRoles.npaAdmin);
    if (hasUserRoleAdministrator) return true;

    // Редактировать страницу "Сведения" может Роль ОИБ = Сотрудник ПУ И если у пользователя  Есть Активная Задача.
    const puPmPhase = [RoutePhaseTypes.puPm, RoutePhaseTypes.puPmInitiation, RoutePhaseTypes.puPmApprovement];
    const hasUserRoleDesignAndPublicationInternal = this.current_user_roles.has(OibRoles.designAndPublicationInternal);

    const currentUserRolePuPmEmployee = getLastPointForCurrentUser?.employee.roles.includes(OibRoles.puEmployee);
    const currentUserLastPointCompleted = [
      VertexState.active,
      VertexState.inactive,
      VertexState.waitingForApprovement,
    ].includes(getLastPointForCurrentUser?.state);
    const isEditAndIsLoadingInfo = !isEdit && !isConsiderationInfoLoading;

    if (hasUserRoleDesignAndPublicationInternal) {
      return true;
    }

    if (
      getLastPointForCurrentUser &&
      !puPmPhase.includes(getLastPointForCurrentUser?.phaseTypeId) &&
      isEditAndIsLoadingInfo
    ) {
      return true;
    }
    if (!currentUserLastPointCompleted) {
      return false;
    }
    if (currentUserRolePuPmEmployee && isEditAndIsLoadingInfo) {
      return true;
    }
  }
}
