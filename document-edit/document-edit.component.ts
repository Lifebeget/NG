import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { ToastDocumentModel } from '@appCore/models/toast-document.model';
import { FormsHelperService } from '@appCore/services/form-helper/form-helper.service';
import { ReduxService } from '@appCore/services/redux/redux.service';
import { ToastsDocumentService } from '@appCore/services/toasts/toasts-document.service';
import { getUserInfo } from '@appCore/store/user-info';
import {
  DocumentPackageGetConsiderationInfo,
  DocumentPackageGetRevisionTypeInfo,
  DocumentPackageResetChangableDocument,
  DocumentPackageResetRevisionTypeInfo,
  DocumentPackageSetSnapshotChangableDocument,
  DocumentPackageUpdateSuccess,
} from '@appDocuments/core/store/document-package/document-package.actions';
import { SavePackageMapperService } from '@appDocuments/document-create/services/save-package-mapper.service';
import { ResponseToFormValueMapperService } from '@appDocuments/document-edit/services/response-to-form-value-mapper.service';
import { getToggleLoader, getToggleSuccessPopup } from '@appDocuments/document-edit/store';
import { ToggleLoader, ToggleSuccessSavePopup } from '@appDocuments/document-edit/store/document-edit-page.actions';
import { CompositionDocumentsCopiesActionReset } from '@appDocuments/documents-composition/store/composition-documents-copies/composition-documents-copies.actions';
import { getDocumentRouteStoreData } from '@appDocuments/documents-route/store/document-route';
import { DocumentRouteGet } from '@appDocuments/documents-route/store/document-route/document-route.actions';
import { DocumentPageMode } from '@appDocuments/shared/enums/document-page-mode.enum';
import { DevelopmentReasonFormModel } from '@appDocuments/shared/models/development-reason-form.model';
import { DocumentPackageMainFormModel } from '@appDocuments/shared/models/document-package-main-form.model';
import { ReviewDateChangeRequestModel } from '@appDocuments/shared/models/review-date-change-request.model';
import { DocumentCreateFormService } from '@appDocuments/shared/services/document-create-form.service';
import { UserHelperService } from '@appDocuments/shared/services/user-helper.service';
import { DocumentPackageApiService } from '@appShared/api/document-package/document-package.api.service';
import { DocumentPackageConsiderationInfoModel } from '@appShared/api/document-package/models/document-package/document-package-consideration-info.model';
import { ReviewDateApiService } from '@appShared/api/review-dates/review-dates.api.service';
import { DocumentSaveStatuses } from '@appShared/enums/documents-save-statuses.enum';
import { OibRoles } from '@appShared/enums/oib-roles.enum';
import { DatesService } from '@appShared/services/dates.service';
import { DictionaryModel } from '@models/base/dictionary.model';
import { DocumentPackageRequestModel } from '@models/document-package/document-package-request.model';
import { DocumentPackageResponseModel } from '@models/document-package/document-package-response.model';
import { RelatedDocumentsModel } from '@models/document-package/related-documents/related-documents.model';
import { ProjectGroupType } from '@models/enums/project-group-type.enum';
import { StoreLoadStatus } from '@models/enums/store-load-status.enum';
import { UserInfoResponseModel } from '@models/user-info.model';
import { DocumentPackagePermissions } from 'app/permissions/enums/document-package-permissions.enum';
import { PermissionType } from 'app/permissions/enums/permission-type.enum';
import { PermissionModel } from 'app/permissions/models/permission.model';
import { Observable, Subject } from 'rxjs';
import { filter, first, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import * as fromDocumentPackageStore from '../core/store/document-package';
import {
  getDocumentPackageChangableDocuments,
  getDocumentPackageConsiderationInfo,
  getDocumentPackageConsiderationInfoStatus,
  getDocumentPackageRevisionTypeInfo,
} from '../core/store/document-package';
import { CompositionDocumentsActionLoad } from '../documents-composition/store/composition-documents/composition-documents.actions';
import { DocumentsSidebarLogicService } from '@appDocuments/core/components/documents-sidebar/documents-sidebar-logic.service';
import { VertexResponseModel } from '../../shared/api/documents-route/models/route-tree.response/vertex.response.model';

@Component({
  selector: 'app-document-edit',
  templateUrl: './document-edit.component.html',
  styleUrls: ['./document-edit.component.scss'],
})
export class DocumentEditComponent implements OnInit, OnDestroy {
  public documentPackage: DocumentPackageResponseModel;
  public packageType: DictionaryModel;
  public toggleSuccessPopup$: Observable<boolean>;
  public toggleLoader$: Observable<boolean>;
  public isEdit: boolean;
  public documentPackageConsiderationInfo: DocumentPackageConsiderationInfoModel[];
  public documentForm: FormGroup;
  public revisionTypeControl: FormControl;
  public permission: PermissionModel;
  public documentPageMode = DocumentPageMode;
  public isReviewDateChangePopupShown: boolean;
  public isReviewDateRequestPopupShown: boolean;
  public reviewDateChangePopupText: string;
  private currentReviewDate: string;

  public canCreateDocumentProject: boolean;
  public canCreateApprovementSheet: boolean;
  public canCharge: boolean;
  public isRouteCompleted = false;
  public approvementSheetTitle: string;
  public toggleLeavePopup: boolean;
  public leaveUrl: string;
  public canDeactivate = true;
  public isCreateDocumentProject = true;
  public isCanViewHistory = false;
  public isLocalProjectTypeCreated: boolean;
  public isProjectTypeDisabled = false;
  public isConsiderationInfoLoading$: Observable<boolean>;
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  private relatedDocuments: RelatedDocumentsModel;
  private userInfo: UserInfoResponseModel;
  public findLastPointForCurrentUser: VertexResponseModel;

  constructor(
    private redux: ReduxService,
    private formBuilder: FormBuilder,
    private documentCreateForm: DocumentCreateFormService,
    private formHelper: FormsHelperService,
    private documentPackageApi: DocumentPackageApiService,
    private responseToFormValueMapper: ResponseToFormValueMapperService,
    protected savePackageMapper: SavePackageMapperService,
    public toastsService: ToastsDocumentService,
    private route: ActivatedRoute,
    private userHelper: UserHelperService,
    private reviewDatesApiService: ReviewDateApiService,
    private dateService: DatesService,
    private router: Router,
    private asideLogic: DocumentsSidebarLogicService
  ) {}

  /** Проверка закрытия страницы браузера*/
  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(_: Event) {
    if (this.isEdit) {
      return false;
    }
  }

  ngOnInit() {
    this.redux.dispatchAction(new DocumentPackageGetConsiderationInfo());
    this.revisionTypeControl = new FormControl({ value: null, disabled: true });
    this.relatedDocuments = {
      affectedDocuments: [],
      affectingDocuments: [],
      links: [],
    };
    this.subscribeToGetDocumentPackage();
    this.subscribeToGetDocumentPackageConsiderationInfo();
    this.subscribeToGetRelatedDocuments();
    this.getToggleSubscriptions();
    this.createDocumentForm();
    this.getUserPermissions();
    this.formHelper.markAsTouchedDeep(this.documentForm);
    this.subscribeToGetDocumentRevisionTypeInfo();
    this.subscribeToGetDocumentPackageConsiderationInfoStatus();
    this.subscribeToRouterNavigationStart();
    this.getPointsRouteCurrentUser();

    this.route.params.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
      this.redux.dispatchAction(new DocumentPackageGetRevisionTypeInfo());
    });

    this.redux.dispatchAction(new DocumentRouteGet(this.documentPackage.id));

    this.userHelper
      .isRouteLoadedAsync()
      .pipe(
        filter((i) => !!i),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe(() => {
        this.canCreateDocumentProject = this.userHelper.canCreateDocumentProject();
        this.canCreateApprovementSheet = this.userHelper.canCreateApprovementSheet();
        this.canCharge = this.userHelper.canCharge();
        this.approvementSheetTitle = this.userHelper.getApprovementSheetTitle();
        this.isRouteCompleted = this.userHelper.isRouteCompleted();
        this.findLastPointForCurrentUser = this.userHelper.findLastPointsForCurrentUser();
      });

    this.isLocalProjectTypeCreated =
      this.mainForm.get('projectType').value.projectGroupType.id === ProjectGroupType.LOCAL_LEGAL_ACTS;

    this.asideLogic.setProjectGroupId(this.getProjectGroupTypeID());
  }

  ngOnDestroy() {
    this.redux.dispatchAction(new DocumentPackageResetRevisionTypeInfo());
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  private getProjectGroupTypeID(): number {
    return this.documentForm?.value.main.projectType.projectGroupType.id;
  }

  /** Форма с основными данными */
  public get mainForm() {
    return this.documentForm.get('main') as FormGroup;
  }

  public startEditForm(): void {
    const { plannedReviewDate } = this.documentForm.getRawValue().reviewDate;
    this.isEdit = true;
    this.canDeactivate = false;
    this.documentForm.enable();

    if (this.isProjectTypeDisabled) {
      this.mainForm.get('projectType').disable();
    }

    this.currentReviewDate = plannedReviewDate;

    this.documentForm.get('reviewDate.reviewReason').disable();
    this.documentForm.get('reviewDate.plannedReviewDate').disable();
    this.documentForm.get('reviewDate.approvalForm').disable();

    this.redux.dispatchAction(new DocumentPackageSetSnapshotChangableDocument());
  }

  public onEditCancel(): void {
    this.redux.dispatchAction(new DocumentPackageResetChangableDocument());
    this.disableEditMode();
  }

  public disableEditMode(): void {
    this.createDocumentForm();
    this.isEdit = false;
    this.canDeactivate = true;
  }

  public onEditSave(): void {
    if (this.documentForm.invalid) {
      this.formHelper.markAsTouchedDeep(this.documentForm);
      return;
    }

    const request = this.prepareToUpdateDocumentPackage();
    this.updateDocumentPackage(request);
    this.redux.dispatchAction(new CompositionDocumentsCopiesActionReset());
  }

  public onSuccessSavePopupClose(): void {
    this.redux.dispatchAction(new ToggleSuccessSavePopup(false));
  }

  public getDevelopmentReasonSavedFormValue(): DevelopmentReasonFormModel {
    return this.responseToFormValueMapper.mapDevelopmentReason(this.documentPackage);
  }

  public getMainFormSavedValue(): DocumentPackageMainFormModel {
    return this.responseToFormValueMapper.mapMainInfo(this.documentPackage);
  }

  public charge(): void {
    this.userHelper.charge();
  }

  public createDocumentProject(): void {
    this.isCreateDocumentProject = false;
    this.userHelper.createDocumentProject();
  }

  public createApprovementSheet(): void {
    this.userHelper.createApprovementSheet();
  }

  public sendReviewDateChangeRequest(reviewDate: ReviewDateChangeRequestModel): void {
    this.reviewDatesApiService.postReviewDateChangeRequest(reviewDate).subscribe(() => {
      const oldPlannedDate = this.documentPackage.reviewDate.plannedReviewDate;
      const formValue = this.documentForm.value;
      formValue.reviewDate.plannedReviewDate = oldPlannedDate;
      const updateReq = this.savePackageMapper.mapFormToSaveRequest(
        formValue,
        this.relatedDocuments,
        this.documentPackage.id,
        this.documentPackage.packageNumber
      );
      this.updateDocumentPackage(updateReq);
    });
  }

  /** При подтверждении ухода со страницы */
  public onPageLeave(): void {
    this.onEditCancel();
    this.router.navigateByUrl(this.leaveUrl);
  }

  private createDocumentForm(): void {
    const mainFormValue = this.getMainFormSavedValue();
    const developmentReasonFormValue = this.getDevelopmentReasonSavedFormValue();
    const reviewDateFormValue = this.responseToFormValueMapper.mapReviewDate(
      this.documentPackage,
      this.documentPackageConsiderationInfo
    );

    this.documentForm = this.formBuilder.group({
      main: this.documentCreateForm.createDocumentPackageMainForm(mainFormValue, true),
      developmentReason: this.documentCreateForm.createDevelopmentReasonForm(
        developmentReasonFormValue,
        true,
        !!developmentReasonFormValue.mayorAgreement
      ),
      reviewDate: this.documentCreateForm.createReviewDateForm(reviewDateFormValue, true),
      presentation: this.documentCreateForm.createPresentationForm(true),
    });
  }

  protected updateDocumentPackage(req: DocumentPackageRequestModel) {
    this.documentPackageApi
      .update(req)
      .pipe(first())
      .subscribe(
        (res) => {
          this.redux.dispatchAction(new ToggleLoader(false));
          this.redux.dispatchAction(new DocumentPackageUpdateSuccess(res));
          this.toastsService.addToast(this.getEditSuccessMessage());
          this.disableEditMode();
        },
        () => {
          this.redux.dispatchAction(new ToggleLoader(false));
          this.toastsService.addToast({ title: DocumentSaveStatuses.editError });
        }
      );
  }

  private getEditSuccessMessage(): ToastDocumentModel {
    let message = DocumentSaveStatuses.editSuccess;

    if (this.isReviewDateChanged()) {
      message = DocumentSaveStatuses.editSuccessDateIsTransferred;
    }

    return { title: message };
  }

  private isReviewDateChanged(): boolean {
    const { plannedReviewDate } = this.documentForm.getRawValue().reviewDate;

    if (!plannedReviewDate && !this.currentReviewDate) {
      return false;
    }

    return !this.dateService.isEqualDates(plannedReviewDate, this.currentReviewDate);
  }

  /** Подгружаем данные о типе редакции документа */
  private subscribeToGetDocumentRevisionTypeInfo(): void {
    this.redux
      .selectStore(getDocumentPackageRevisionTypeInfo)
      .pipe(
        filter((info) => !!(info && info.statusValue)),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((info) => {
        this.revisionTypeControl.setValue(info.statusValue.name);
      });
  }

  private getToggleSubscriptions(): void {
    this.toggleLoader$ = this.redux.selectStore(getToggleLoader);
    this.toggleSuccessPopup$ = this.redux.selectStore(getToggleSuccessPopup);
  }

  private getUserPermissions(): void {
    this.permission = {
      permissions: DocumentPackagePermissions.DOCUMENT_PACKAGE_EDIT,
      type: PermissionType.documentPackage,
      id: `${this.documentPackage.id}`,
    };
  }

  private prepareToUpdateDocumentPackage(): DocumentPackageRequestModel {
    this.redux.dispatchAction(new ToggleLoader(true));

    this.documentForm.value.reviewDate = this.documentForm.getRawValue().reviewDate;

    return this.savePackageMapper.mapFormToSaveRequest(
      this.documentForm.value,
      this.relatedDocuments,
      this.documentPackage.id,
      this.documentPackage.packageNumber
    );
  }

  /** Получение информации о рассмотрениях пакета документов */
  private subscribeToGetDocumentPackageConsiderationInfo(): void {
    this.redux
      .selectStore(getDocumentPackageConsiderationInfo)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((info) => {
        this.documentPackageConsiderationInfo = info;
        setTimeout(() => {
          this.createDocumentForm();
        });
      });
  }

  private subscribeToGetRelatedDocuments(): void {
    this.redux
      .selectStore(getDocumentPackageChangableDocuments)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((docs) => {
        this.relatedDocuments.affectedDocuments = docs;
      });
  }

  private subscribeToGetDocumentPackage(): void {
    this.redux
      .selectStore(fromDocumentPackageStore.getDocumentPackage)
      .pipe(
        filter((documentPackage) => !!documentPackage),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((documentPackage: DocumentPackageResponseModel) => {
        this.documentPackage = documentPackage;
        this.packageType = documentPackage.packageType;
        this.redux.dispatchAction(new CompositionDocumentsActionLoad({ documentPackageId: documentPackage.id }));
      });
  }

  /** Получение на статус загрузки информации о рассмотрениях пакета документов */
  private subscribeToGetDocumentPackageConsiderationInfoStatus(): void {
    this.isConsiderationInfoLoading$ = this.redux
      .selectStore(getDocumentPackageConsiderationInfoStatus).pipe(
        map((res) => res === StoreLoadStatus.inProgress)
      );
  }

  /** Подписка на событие старта навигации */
  private subscribeToRouterNavigationStart(): void {
    this.router.events
      .pipe(
        filter((val) => val instanceof NavigationStart),
        filter(() => this.isEdit && !this.canDeactivate),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((val: NavigationStart) => {
        this.leaveUrl = val.url;
        this.toggleLeavePopup = true;
      });
  }

  private checkCanViewHistory(user: UserInfoResponseModel): boolean {
    const whoCanViewHistory = [OibRoles.npaAdmin, OibRoles.oauEmployee, OibRoles.pzOAUEmployee];
    const userRoles = user.currentUser.roles;

    return userRoles.some((role: OibRoles) => whoCanViewHistory.includes(role));
  }

  private getPointsRouteCurrentUser(): void {
    this.redux
      .selectStore(getUserInfo)
      .pipe(
        filter((user) => !!user),
        tap((userInfo) => (this.userInfo = userInfo)),
        switchMap(() => this.redux.selectStore(getDocumentRouteStoreData)),
        filter((route) => !!route),
        map((route) => {
          const isController = route.controllers.filter(
            (v) => v?.employee?.id === this.userInfo.currentUser.employee.id
          );
          const isEmployee = route.vertexes.filter((v) => v?.employee?.id === this.userInfo.currentUser.employee.id);
          const isCoauthor = route.vertexes.filter(
            (v) => v.employee && v?.coauthors.filter((c) => c?.id === this.userInfo.currentUser.employee.id)
          );
          return [...isEmployee, ...isController, ...isCoauthor];
        }),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((pointsUser) => {
        this.isCanViewHistory = this.checkCanViewHistory(this.userInfo) || !!pointsUser.length;
      });
  }

  public onGetProjectTypes(hasAvailableProjectTypes: boolean): void {
    if (hasAvailableProjectTypes) {
      this.isProjectTypeDisabled = false;
      return;
    }
    this.isProjectTypeDisabled = true;
    this.mainForm.get('projectType').disable();
  }
}
