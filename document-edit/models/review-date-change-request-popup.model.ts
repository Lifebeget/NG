import { ReviewDateFormModel } from '../../shared/models/review-date-form.model';

export class ReviewDateChangeRequestPopupModel {
  documentPackageId: number;
  originalReviewDate: ReviewDateFormModel;
  newReviewDate: ReviewDateFormModel;
}
