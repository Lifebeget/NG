import { DocumentPackageResponseModel } from '@models/document-package/document-package-response.model';
import { ResponseToFormValueMapperService } from './response-to-form-value-mapper.service';
import { ProjectTypeModel } from '@models/document-package/project-type.model';

describe('ResponseToFormValueMapperService', () => {
  let service: ResponseToFormValueMapperService;
  let documentPackage: DocumentPackageResponseModel;

  beforeEach(() => {
    service = new ResponseToFormValueMapperService();
    documentPackage = getMockDocumentPackageResponse();
  });

  it('should map document package to main info', () => {
    const mainInfo = service.mapMainInfo(documentPackage);

    expect(mainInfo.projectType).toEqual(documentPackage.main.projectType);
    expect(mainInfo.status).toEqual(documentPackage.main.status);
    expect(mainInfo.name).toEqual(documentPackage.main.name);
    expect(mainInfo.description).toEqual(documentPackage.main.description);
  });

  it('should map document package to development reason', () => {
    const developmentReason = service.mapDevelopmentReason(documentPackage);

    expect(developmentReason).toMatchSnapshot();
  });

  it('should map document package to review date', () => {
    const reviewDate = service.mapReviewDate(documentPackage, null);

    expect(reviewDate).toMatchSnapshot();
  });
});

/** Моковый респонс */
function getMockDocumentPackageResponse(): DocumentPackageResponseModel {
  return {
    id: 15,
    businessVersion: null,
    createUser: null,
    realCreateUser: null,
    isActingUser: false,
    relatedDocuments: null,
    categories: [],
    packageType: { id: 1, name: 'Пакет документа' },
    packageNumber: '6726-1323-8233',
    main: {
      projectType: { id: 1, name: 'Постановление ПМ', shortName: 'ПП' } as ProjectTypeModel,
      status: { id: 1, name: 'Плановый' },
      name: 'Название пакета',
      description: 'Краткое описание',
    },
    developmentReason: {
      comment: 'comment',
      instruction: null,
      mayorAgreement: null,
      townPlanningMoscowCommission: null,
      commissionProtocolNumber: null,
      commissionProtocolDate: null,
    },
    reviewDate: {
      id: null,
      plannedReviewDate: '2018-04-24T00:00:00+03:00',
      approvalForm: {
        id: 3,
        name: 'Не подлежит рассмотрению на заседании ПМ',
        fullName: 'Не подлежит рассмотрению на заседании Правительства Москвы',
      },
      reviewReason: 'Обоснование даты рассмотрения',
    },
    message: 'Сообщение',
  };
}
