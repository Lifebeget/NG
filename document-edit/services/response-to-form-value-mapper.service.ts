import { Injectable } from '@angular/core';
import { ApprovalFormValuesIds } from '@appCore/enums/approval-form-values.enum';
import { APPROVAL_FORM_VALUES_FULL_DATA } from '@appDocuments/shared/const/approval-form-values-full-data.const';
import { ConsiderationTypes } from '@appMain/calendar-consideration/enums/consideration-types.enum';
import { DocumentPackageConsiderationInfoModel } from '@appShared/api/document-package/models/document-package/document-package-consideration-info.model';
import { DocumentPackageResponseModel } from '@models/document-package/document-package-response.model';
import moment from 'moment';
import { DevelopmentReasonFormModel } from '../../shared/models/development-reason-form.model';
import { DocumentPackageMainFormModel } from '../../shared/models/document-package-main-form.model';
import { ReviewDateFormModel } from '../../shared/models/review-date-form.model';

/** Сервис для мапинга данных в формат формы для заполнения */
@Injectable()
export class ResponseToFormValueMapperService {
  constructor() {}

  /** Основная информация */
  public mapMainInfo(documentPackage: DocumentPackageResponseModel): DocumentPackageMainFormModel {
    return documentPackage.main;
  }

  /** Мапинг блока основания разработки */
  public mapDevelopmentReason(documentPackage: DocumentPackageResponseModel): DevelopmentReasonFormModel {
    return documentPackage.developmentReason;
  }

  /** Мапинг дат рассмотрения */
  public mapReviewDate(
    documentPackage: DocumentPackageResponseModel,
    considerationInfo: DocumentPackageConsiderationInfoModel[]
  ): ReviewDateFormModel {
    /** Ищем ПП заседание в информации о рассмотрениях ПД */
    const mettingItem =
      considerationInfo &&
      considerationInfo.find(
        (item) =>
          item.agendaPackageType &&
          (item.agendaPackageType.id === ConsiderationTypes.meetingPM ||
            item.agendaPackageType.id === ConsiderationTypes.meetingPPM)
      );
    const source = documentPackage.reviewDate;

    if (mettingItem) {
      const approvalForm =
        mettingItem.agendaPackageType.id === ConsiderationTypes.meetingPM
          ? APPROVAL_FORM_VALUES_FULL_DATA.pmMeeting
          : APPROVAL_FORM_VALUES_FULL_DATA.ppmMeeting;

      return {
        id: source ? source.id : null,
        plannedReviewDate: mettingItem.agendaPackageDate,
        approvalForm: approvalForm,
        reviewReason: this.getMeetingPmReviewReason(mettingItem, source),
      };
    }

    /** Не показываем старые данные по форме утверждения заседание ПМ и заседание ППМ,
     * в случае если пакет на данный момент не вынесен как вопрос ни в одном из заседаний ПМ пакета повестки (!mettingPmItem) */
    const isMeetingPmApprovalForm =
      source &&
      source.approvalForm &&
      (source.approvalForm.id === ApprovalFormValuesIds.pmMeeting ||
        source.approvalForm.id === ApprovalFormValuesIds.ppmMeeting);

    if (!source || isMeetingPmApprovalForm) {
      return null;
    }

    const result = {
      ...documentPackage.reviewDate,
    };

    return result;
  }

  /** Метод для показа пользователю "обоснования даты рассмотрения" только в том случае,
   * если дата рассмотрения сохраненная в main модуле совпадает с датой Заседания приходящей в информации из модуля календаря */
  private getMeetingPmReviewReason(
    pmMeetingInfo: DocumentPackageConsiderationInfoModel,
    reviewDateInfo: ReviewDateFormModel
  ): string {
    if (!reviewDateInfo) {
      return null;
    }

    const pmDate = pmMeetingInfo.agendaPackageDate;
    const reviewDate = reviewDateInfo.plannedReviewDate;

    return moment(pmDate).isSame(reviewDate, 'day') ? reviewDateInfo.reviewReason : null;
  }
}
